var gulp = require('gulp'),
	env = require('gulp-env'),
	uglify = require('gulp-uglify'),
	source = require('vinyl-source-stream'),
	buffer = require('vinyl-buffer'),
	browserify = require('browserify'),
	reactify = require('reactify'),
	livereload = require('gulp-livereload'),
	concat = require('gulp-concat'),
	sass = require('gulp-sass'),
	cleanCSS = require('gulp-clean-css'),
	rename = require('gulp-rename'),
	es = require('event-stream'),
	glob = require('glob');

var vendors = [
	'react',
	'react-dom',
	'jquery',
	'object-assign',
	'events',
	'flux',
	'jsonwebtoken',
	'key-del'
];

gulp.task('build:vendor', function() {
	var bundler = browserify();

	vendors.forEach(function(vendor) {
		bundler.require(vendor);
	});

	var g = bundler.bundle()
		.pipe(source('vendor.js'));

	if (process.env.NODE_ENV === 'prod') {
		g = g.pipe(buffer())
			.pipe(uglify());
	}
		
	g.pipe(gulp.dest('.'));
});

// gulp.task('build:app', function() {
//     var files = [
//         'src/index.jsx',
//         'src/upload.jsx'
//     ];

//     var tasks = files.map(function(entry) {
//         return browserify({ entries: [entry] })
//             .bundle()
//             .pipe(source(entry))
//             // rename them to have "bundle as postfix"
//             .pipe(rename({
//                 extname: '.bundle.js'
//             }))
//             .pipe(gulp.dest('./dist'));
//         });
//     // create a merged stream
//     return es.merge.apply(null, tasks);
// });

gulp.task('build:app', function() {
	glob('src/*.jsx', function(err, files) {
		if (err) {
			// done(err);
			console.log(err);
		}

		var tasks = files.map(function(entry) {
			var g = browserify({ entries: [entry] })
				.external(vendors)
				.transform(reactify)
				.bundle()
				.on('error', function(err){
					console.log(err.message);
					this.emit('end');
				}).pipe(source(entry));

			if (process.env.NODE_ENV === 'prod') {
				g = g.pipe(buffer())
					.pipe(uglify());
			}

			return g.pipe(rename(function(path) {
					path.dirname = '';
					path.basename += '.bundle';
					path.extname = '.js';
				}))
				.pipe(gulp.dest('.'));
		});

		es.merge(tasks).on('end', function() {
			// console.log('end');
		});
	});
});

// gulp.task('build:app', function() {
// 	var g = browserify('src/index.jsx')
// 		.external(vendors)
// 		.transform(reactify)
// 		.bundle()
// 		.on('error', function(err){
// 			console.log(err.message);
// 			this.emit('end');
// 		})
// 		.pipe(source('bundle.js'));

// 	if (process.env.NODE_ENV === 'prod') {
// 		g = g.pipe(buffer())
// 			.pipe(uglify());
// 	}

// 	g.pipe(gulp.dest('.'));
// });

gulp.task('combine-scss', function() {
	var g = gulp.src('src/scss/*.scss')
		.pipe(sass().on('error', sass.logError));

	if (process.env.NODE_ENV === 'prod') {
		g = g.pipe(cleanCSS());
	}
	
	g.pipe(concat('bundle.css'))
		.pipe(gulp.dest('.'));
});

gulp.task('set:dev', function() {
	gulp.envs = env.set({
		NODE_ENV: 'dev'
	});
});

gulp.task('set:prod', function() {
	gulp.envs = env.set({
		NODE_ENV: 'prod'
	});
});

gulp.task('build:prod', [
	'set:prod',
	'combine-scss', 'build:vendor', 'build:app'
]);

gulp.task('build', [
	'set:dev',
	'combine-scss', 'build:vendor', 'build:app'
]);

gulp.task('watch', function() {
	livereload.listen();
	gulp.watch('src/scss/*.scss', ['combine-scss']);
	gulp.watch(['src/*.jsx', 'src/js/*.js', 'src/jsx/*.jsx'], ['build:app']);
	gulp.watch(['*.css', '*.js', '*.html']).on('change', livereload.changed);
});

gulp.task('prod', ['build:prod', 'watch']);
gulp.task('default', ['build', 'watch']);
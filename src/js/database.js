var pg = require('pg');

var mapper = require('./mapper.js');

pg.defaults.ssl = true;
const dbUrl = process.env.DATABASE_URL || 'postgres://sebpdrztnldbmd:Z9jCz4yNeuv2R1pKKquZMaq1wx@ec2-54-243-226-46.compute-1.amazonaws.com:5432/d55udashaek2c5';

var connect = function() {
	console.log('[DB] connecting to database');

	return new Promise(function(resolve, reject) {
		pg.connect(dbUrl, function(err, client, done) {
			if (err) {
				reject(err);
			} else {
				resolve(client);
			}
			done();
		});
	});
};

var run = function(query) {
	return new Promise(function(resolve, reject) {
		connect().then(function(client) {
			var rows = [];

			console.log('[DB] run query = <' + query + '>');

			client.query(query)
				.on('row', function(row) {
					rows.push(row);
				})
				.on('end', function() {
					var count = rows.length;
					console.log('[DB] query ended with ' + count + ' row' + (count > 1 ? 's' : ''));
					resolve(rows);
				});
		}, function(err) {
			reject(err);
		})
		.catch(function(err) {
			reject(err);
		});
	});
};

var getSecret = function() {
	var query = 'select * from secrets where valid = 1 order by updated_at desc limit 1';

	return new Promise(function(resolve, reject) {
		run(query).then(function(rows) {
			if (rows.length === 0) {
				reject('No secret from database');
			} else {
				resolve(rows[0]);
			}
		}, function(err) {
			reject(err);
		});
	});
};

var getFilters = function() {
	var query = 'select title, color from filters where deleted = 0 order by filter_id';

	return new Promise(function(resolve, reject) {
		run(query).then(function(rows) {
			if (rows.length === 0) {
				reject('No filter from database');
			} else {
				resolve(rows);
			}
		}, function(err) {
			reject(err);
		});
	});
};

var getUser = function(user) {
	var query = 'select * from users where valid = 1 and ';

	query += 'username = ' + "'" + user.username + "' and ";
	query += 'password = ' + "'" + user.password + "'";

	return new Promise(function(resolve, reject) {
		run(query).then(function(rows) {
			if (rows.length === 0) {
				reject('No matched user from database');
			} else {
				resolve(rows[0]);
			}
		}, function(err) {
			reject(err);
		});
	});
};

var setUserLogInDate = function(user) {
	var query = 'update users set last_login_at = localtimestamp where user_id = ' + user.user_id;

	return new Promise(function(resolve, reject) {
		run(query).then(function(rows) {
			resolve();
		}, function(err) {
			reject(err);
		});
	});
};

var updateTerms = function(_terms) {
	function _updateTerm(term) {
		var query = '';

		if (term.term_id === undefined || term.term_id === null) {
			query += 'insert into terms (type, keyword, title, description) values ';

			query += '(';

			query += term.type + ', ';
			query += "'" + term.keyword + "'" + ', ';
			query += "'" + term.title + "'" + ', ';

			if (term.description === undefined || term.description === null || term.description.length === 0) {
				query += 'null';
			} else {
				query += "'" + term.description + "'";
			}

			query += ')';
		} else {
			query += 'update terms set ';

			query += 'updated_at = localtimestamp, ';
			query += 'deleted = ' + term.deleted + ', ';

			query += 'type = ' + term.type + ', ';
			query += 'keyword = ' + "'" + term.keyword + "'" + ', ';
			query += 'title = ' + "'" + term.title + "'" + ', ';

			query += 'description = ';
			if (term.description === undefined || term.description === null) {
				query += 'null';
			} else {
				query += "'" + term.description + "'";
			}

			query += ' where term_id = ' + term.term_id;
		}

		query += ' returning term_id';

		return new Promise(function(resolve, reject) {
			run(query).then(function(rows) {
				if (rows.length === 0) {
					reject('[DB] essential column "term_id" not retreived');
				} else {
					resolve(rows[0].term_id);
				}
			}, function(err) {
				reject(err);
			});
		});
	};

	function _updateTags(tags, term_id, isNew) {
		var count = tags.length;

		if (tags === undefined || tags === null || count === 0) {
			return Promise.resolve();
		}

		var query = '';

		if (isNew === true) {
			query += 'insert into tags (term_id, name) values ';

			tags.forEach(function(tag, index) {
				query += '(';

				query += term_id + ', ';
				query += "'" + tag.name + "'";

				query += ')';

				if (index < count - 1) {
					query += ', ';
				}
			});
		} else {
			query += 'update tags as t set updated_at = c.updated_at, deleted = c.deleted, name = c.name from (values ';

			tags.forEach(function(tag, index) {
				query += '(';

				query += tag.tag_id + ', ';

				query += 'localtimestamp, ';
				query += tag.tag_deleted + ', ';

				query += "'" + tag.name + "'";

				query += ')';

				if (index < count - 1) {
					query += ', ';
				}
			});

			query += ') as c(tag_id, updated_at, deleted, name)';
			query += ' where c.tag_id = t.tag_id';
		}

		return new Promise(function(resolve, reject) {
			run(query).then(function(rows) {
				resolve();
			}, function(err) {
				reject(err);
			});
		});
	};

	function _updateTermTag(set) {
		var _term_id;
		console.log(set);

		return new Promise(function(resolve, reject) {
			console.log('1');
			_updateTerm(set.term)
				.then(function(term_id) {
					console.log('2');
					_term_id = term_id;

					var tagsToInsert = [];
					set.tags.forEach(function(tag) {
						if (tag.tag_id === undefined) {
							tagsToInsert.push(tag);
						}
					});

					return _updateTags(tagsToInsert, _term_id, true);
				}, function(err) {
					console.log('3');
					reject({ code: 1, result: err });
				}).then(function() {
					console.log('4');
					var tagsToUpdate = [];
					set.tags.forEach(function(tag) {
						if (tag.tag_id !== undefined) {
							tagsToUpdate.push(tag);
						}
					});

					return _updateTags(tagsToUpdate, _term_id, false);
				}, function(err) {
					console.log('5');
					reject({ code: 1, result: err });
				}).then(function() {
					console.log('6');
					resolve({ code: 0, result: 'success' });
				}, function(err) {
					console.log('7');
					reject({ code: 1, result: err });
				});
		});
	};

	var terms = [];

	if (_terms instanceof Array) {
		terms = _terms;
	} else {
		terms.push(_terms);
	}

	var promises = [];
	mapper.unmap(terms).forEach(function(set) {
		promises.push(_updateTermTag(set));
	});

	return Promise.all(promises);
};

var getLogs = function() {
	var query = 'select * from logs where deleted = 0 order by created_at desc';
	return run(query);
};

var postLogs = function(logs) {
	function insertLog(log) {
		var query = 'insert into logs (version, description) values ';

		query += '(';

		query += "'" + log.version + "'" + ', ';
		query += "'" + log.description + "'";

		query += ')';

		query += ' returning *';

		return new Promise(function(resolve, reject) {
			run(query).then(function(rows) {
				resolve({
					type: 0,
					row: rows[0]
				});
			}, function(err) {
				reject(err);
			});
		});
	};

	function updateLog(log) {
		if (log.deleted === undefined && log.version === undefined && log.description === undefined) {
			return Promise.resolve();
		}

		var query = 'update logs set ';

		if (log.deleted !== undefined) {
			query += 'deleted = ' + log.deleted + ', ';
		}

		if (log.version !== undefined) {
			query += 'version = ' + "'" + log.version + "'" + ', ';
		}

		if (log.description !== undefined) {
			query += 'description = E' + "'" + log.description + "'" + ', ';
		}

		query += 'updated_at = localtimestamp'

		query += ' where log_id = ' + log.id;
		query += ' returning *';

		return new Promise(function(resolve, reject) {
			run(query).then(function(rows) {
				resolve({
					type: 1,
					row: rows[0]
				});
			}, function(err) {
				reject(err);
			});
		});
	};

	var promises = [];

	if (logs instanceof Array) {
		logs.forEach(function(log) {
			if (log.id === undefined) {
				promises.push(insertLog(log));
			} else {
				promises.push(updateLog(log));
			}
		});
	} else {
		if (logs.id === undefined) {
			promises.push(insertLog(logs));
		} else {
			promises.push(updateLog(logs));
		}
	}

	return new Promise(function(resolve, reject) {
		Promise.all(promises).then(function(results) {
			var result = {}

			results.forEach(function(_result) {
				if (_result.type === 0) {
					if (result.inserted === undefined) {
						result.inserted = [];
					}
					result.inserted.push(_result.row);
				} else if (_result.type === 1) {
					if (result.updated === undefined) {
						result.updated = [];
					}
					result.updated.push(_result.row);
				}
			});

			resolve(result);
		});
	});
};

var getMessages = function() {
	var query = 'select * from messages where deleted = 0 order by created_at desc';
	return run(query);
};

var postMessages = function(messages) {
	function insertMessage(message) {
		var query = 'insert into messages (kind, content) values ';

		query += '(';

		query += message.kind + ', ';
		query += "'" + message.content + "'";

		query += ')';

		query += ' returning *';

		return new Promise(function(resolve, reject) {
			run(query).then(function(rows) {
				resolve({
					type: 0,
					row: rows[0]
				});
			}, function(err) {
				reject(err);
			});
		});
	};

	function updateMessage(message) {
		if (message.kind === undefined && message.read === undefined && message.deleted === undefined && message.content === undefined) {
			return Promise.resolve();
		}

		var query = 'update messages set ';

		if (message.kind !== undefined) {
			query += 'kind = ' + message.kind + ', ';
		}

		if (message.read !== undefined) {
			query += 'read = ' + "'" + message.read + "'" + ', ';
		}

		if (message.deleted !== undefined) {
			query += 'deleted = ' + "'" + message.deleted + "'" + ', ';
		}

		if (message.content !== undefined) {
			query += 'content = E' + "'" + message.content + "'" + ', ';
		}

		query += 'updated_at = localtimestamp'

		query += ' where message_id = ' + message.id;
		query += ' returning *';

		return new Promise(function(resolve, reject) {
			run(query).then(function(rows) {
				resolve({
					type: 1,
					row: rows[0]
				});
			}, function(err) {
				reject(err);
			});
		});
	};

	var promises = [];

	if (messages instanceof Array) {
		messages.forEach(function(message) {
			if (message.id === undefined) {
				promises.push(insertMessage(message));
			} else {
				promises.push(updateMessage(message));
			}
		});
	} else {
		if (messages.id === undefined) {
			promises.push(insertMessage(messages));
		} else {
			promises.push(updateMessage(messages));
		}
	}

	return new Promise(function(resolve, reject) {
		Promise.all(promises).then(function(results) {
			var result = {}

			results.forEach(function(_result) {
				if (_result.type === 0) {
					if (result.inserted === undefined) {
						result.inserted = [];
					}
					result.inserted.push(_result.row);
				} else if (_result.type === 1) {
					if (result.updated === undefined) {
						result.updated = [];
					}
					result.updated.push(_result.row);
				}
			});

			resolve(result);
		});
	});
};

module.exports.run = run;

// utility methods
module.exports.getSecret = getSecret;
module.exports.getFilters = getFilters;
module.exports.getUser = getUser;
module.exports.setUserLogInDate = setUserLogInDate;
module.exports.updateTerms = updateTerms;
module.exports.getLogs = getLogs;
module.exports.postLogs = postLogs;
module.exports.getMessages = getMessages;
module.exports.postMessages = postMessages;

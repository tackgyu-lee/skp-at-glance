var assign = require('object-assign'),
	EventEmitter = require('events').EventEmitter,
	deleteKey = require('key-del');

var Dispatcher = require('./dispatcher.js'),
	Actions = require('./actions.js'),
	Constants = require('./constants.js');

var props = {
	private: {
		hash: null,
		page: 0,
		didFetchVersion: false
	},

	public: {
		shouldScrollToTop: false,
		shouldDismissPopup: false,

		logInFormState: {
			shouldPopUp: false,
			shouldEmpty: false,
			shouldInformFailure: false
		},

		insertFormState: {
			isOpen: false,
			isUpdating: false,
			term: null
		},

		filterState: {
			state: 0, // 0: init, 1: loaded, -1: failed
			index: -1,
			specs: []
		},

		uploaderState: {
			state: 0, // 0: idle, 1: loading, -1: failed
			// hash: null,
			filename: '',
			terms: []
		},

		messageBoxState: {
			isLoading: false,
			messages: [],
			index: -1,
			logMessage: ''
		},

		requestState: {
			type: 0,
			description: '',
			isLoading: false,
			logMessageType: 0,
			logMessage: ''
		},

		isSearching: false,
		isFetchingToken: false,
		isInitiated: false,

		token: null,

		terms: [],
		hasMore: true,
		searchText: '',
		version: '',
		updateLogs: []
	}
};

var util = {
	generateHash: function() {
		return (+new Date() + Math.floor(Math.random() * 999999)).toString(36);
	},

	getMatches: function(searchText) {
		if (typeof searchText !== 'string' || searchText.length === 0) {
			return null;
		}

		var split = searchText.trim().split(' ');

		if (split.length === 0) {
			return null;
		}

		var words = [];

		split.forEach(function(word) {
			if (word.search(',') !== -1) {
				word.split(',').forEach(function(aWord) {
					words.push(aWord.trim());
				});
			} else {
				words.push(word);
			}
		});

		var matches = [];

		words.forEach(function(word) {
			if (word.search('@') === 0) {
				matches.push({
					target: Constants.SEARCH.TARGET.GROUP,
					text: word.substr(1, word.length - 1)
				});
			} else if (word.search('#') === 0) {
				matches.push({
					target: Constants.SEARCH.TARGET.TAG,
					text: word.substr(1, word.length - 1)
				});
			} else {
				matches.push({
					target: Constants.SEARCH.TARGET.NORMAL,
					text: word
				});
			}
		});

		return matches;
	}
};

var Controller = assign({}, EventEmitter.prototype, {
	getVersion: function() {
		if (props.private.didFetchVersion) {
			true;
		}

		$.ajax({
			url: '/version',
			
			success: function(data) {
				props.public.version = data.version;
				this.emitChange();

				props.private.didFetchVersion = true;
			}.bind(this)
		});
	},

	getUpdateLogs: function() {
		$.ajax({
			url: '/log',
			
			success: function(data) {
				props.public.updateLogs = data;
				this.emitChange();
			}.bind(this)
		});
	},

	search: function(searchText, option) {
		var append = false;
		var pending = false;
		var typing = false;
		var fixScroll = false;

		if (option !== undefined) {
			append = option.append === true;
			pending = option.pending === true;
			typing = option.typing === true;
			fixScroll = option.fixScroll === true;
		}

		props.public.searchText = searchText;
		props.public.insertFormState.isOpen = false;

		this.emitChange();

		var hash = util.generateHash();
		props.private.hash = hash;
		
		if (pending === true) {
			return;
		}

		if (typing === true) {
			setTimeout(function() {
				if (hash === props.private.hash) {
					option.typing = false;
					this.search(searchText, option);
				}
			}.bind(this), 350);
			return;
		}

		process.nextTick(function() {
			props.public.isSearching = true;

			if (!fixScroll) {
				props.public.shouldScrollToTop = !fixScroll;
			}

			this.emitChange();

			process.nextTick(function() {
				props.public.shouldScrollToTop = false;
			});

			if (append !== true) {
				props.private.page = 0;
				props.public.terms = [];
				props.public.hasMore = true;
			}

			var matches = util.getMatches(searchText);
			var hash = util.generateHash();
			props.private.hash = hash;

			$.ajax({
				url: '/search',
				method: 'PUT',
				contentType: 'application/json; charset=UTF-8',
				dataType: 'json',
				data: JSON.stringify({
					page: props.private.page,
					matches: matches,
					criteria: Constants.SEARCH.CRITERIA.OR
				}),

				success: function(data) {
					if (hash === props.private.hash) {
						if (data.results.length === 0) {
							props.public.hasMore = false;
						} else {
							data.results.forEach(function(result) {
								props.public.terms.push(result);
							});
						}

						props.private.page++;
						props.public.isSearching = false;

						if (!props.public.isInitiated) {
							props.public.isInitiated = true;
						}

						process.nextTick(function() {
							this.emitChange();
						}.bind(this));
					}
				}.bind(this),
				error: function(err) {
					props.public.isSearching = false;
					this.emitChange();
				}.bind(this)
			});
		}.bind(this));
	},

	getFilters: function(callback) {
		this.resetFilters();
		this.emitChange();

		$.ajax({
			url: '/filter',
			
			success: function(data) {
				var specs = [];

				data.filters.forEach(function(obj) {
					specs.push({
						title: obj.title,
						color: obj.color
					});
				});

				props.public.filterState.state = Constants.FILTER.LOADED;
				props.public.filterState.specs = specs;

				this.emitChange();
			}.bind(this),
			error: function() {
				props.public.filterState.state = Constants.FILTER.FAILED;
				this.emitChange();
			}.bind(this)
		});
	},

	setFilterIndex: function(filterIndex) {
		if (filterIndex !== props.public.filterState.index) {
			props.public.filterState.index = filterIndex;

			var searchText = '';
			if (filterIndex > 0) {
				searchText = '@' + props.public.filterState.specs[filterIndex-1].title;
			}

			process.nextTick(function() {
				this.search(searchText);
			}.bind(this));
		}
	},

	resetFilters: function() {
		props.public.filterState = {
			state: Constants.FILTER.INIT,
			index: 0,
			specs: []
		};
	},

	getTokenFromSession: function() {
		props.public.isFetchingToken = true;
		this.emitChange();

		process.nextTick(function() {
			$.ajax({
				url: '/session',

				success: function(data) {
					props.public.token = data.output;
					this.emitChange();

					this.getMessages();
				}.bind(this),
				error: function() {
					// no session
				},
				complete: function() {
					props.public.isFetchingToken = false;
					this.emitChange();
				}.bind(this)
			});
		}.bind(this));
	},

	popUpLogInForm: function(show) {
		props.public.logInFormState.shouldEmpty = true;
		props.public.logInFormState.shouldPopUp = show;

		this.emitChange();

		process.nextTick(function() {
			props.public.logInFormState.shouldEmpty = false;
		});
	},

	openInsertForm: function(show, option) {
		var clean = false;

		if (option !== undefined) {
			clean = option.clean === true;
		}

		if (clean) {
			props.public.insertFormState.term = null;
		} else if (option !== undefined) {
			if (option.term !== undefined) {
				props.public.insertFormState.term = option.term;
			}
		}

		props.public.insertFormState.isOpen = show;

		if (show) {
			props.public.shouldScrollToTop = true;
		}

		this.emitChange();

		props.public.shouldScrollToTop = false;
	},

	updateTerms: function(terms) {
		props.public.uploaderState.state = 1;
		this.emitChange();

		process.nextTick(function() {
			$.ajax({
				url: '/term',
				method: 'POST',
				contentType: 'application/json; charset=UTF-8',
				dataType: 'json',
				data: JSON.stringify(terms),

				complete: function() {
					console.log('ended');
					location.href = '/';
				}
			});
		});
	},

	logIn: function(username, password) {
		$.ajax({
			url: '/user',
			method: 'PUT',
			contentType: 'application/json; charset=UTF-8',
			dataType: 'json',
			data: JSON.stringify({
				username: username,
				password: password
			}),

			success: function(data) {
				props.public.token = data.token;
				this.emitChange();

				process.nextTick(function() {
					this.popUpLogInForm(false);
					this.getMessages();
				}.bind(this));
			}.bind(this),
			error: function(error) {
				props.public.logInFormState.shouldInformFailure = true;
				this.emitChange();

				process.nextTick(function() {
					props.public.logInFormState.shouldInformFailure = false;
				});
			}.bind(this)
		});
	},

	logOut: function(option) {
		var redirect = false;

		if (option !== undefined) {
			redirect = option.redirect;
		}

		props.public.token = null;
		this.emitChange();

		$.ajax({
			url: '/session',
			method: 'delete',

			complete: function() {
				if (redirect === true) {
					location.href = '/';
				}
			}
		});
	},

	uploadData: function(filename, data) {
		props.public.uploaderState.filename = filename;
		props.public.uploaderState.state = 1;
		props.public.uploaderState.terms = [];

		this.emitChange();

		process.nextTick(function() {
			$.ajax({
				url: '/upload',
				type: 'POST',
				data: data,
				processData: false,
				contentType: false,

				success: function(res){
					res.results.forEach(function(term) {
						props.public.uploaderState.terms.push(term);
					});

					props.public.uploaderState.state = 0;
					this.emitChange();
				}.bind(this),
				error: function(err) {
					props.public.uploaderState.state = -1;
					this.emitChange();
				}.bind(this)
			});
		}.bind(this));
	},

	deleteOnUploadableTerm: function(termIndex) {
		var terms = props.public.uploaderState.terms;

		terms.splice(termIndex, 1);

		if (terms.length === 0) {
			this.emptyTerms();
		} else {
			props.public.uploaderState.terms = terms;
			this.emitChange();
		}
	},

	emptyTerms: function() {
		location.href = '/upload';
	},

	dismissPopup: function() {
		props.public.shouldDismissPopup = true;
		this.emitChange();

		process.nextTick(function() {
			props.public.shouldDismissPopup = false;
		});
	},

	getMessages: function(option) {
		var init = true;

		if (option !== undefined) {
			init = option.init === true;
		}

		var messageBoxState = props.public.messageBoxState;

		messageBoxState.messages = [];

		if (init) {
			messageBoxState.isLoading = true;
			messageBoxState.logMessage = '';
			props.public.messageBoxState = messageBoxState;
			this.emitChange();
		}

		process.nextTick(function() {
			$.ajax({
				url: '/message',

				success: function(data) {
					data.forEach(function(message) {
						messageBoxState.messages.push(message);
					});

					if (messageBoxState.messages.length === 0) {
						messageBoxState.messages = [];
						messageBoxState.logMessage = 'Inbox empty.';
					}
				},
				error: function() {
					messageBoxState.messages = [];
					messageBoxState.logMessage = 'Failed to fetch messages.';
				},
				complete: function() {
					messageBoxState.isLoading = false;
					this.emitChange();
				}.bind(this)
			});
		}.bind(this));
	},

	setMessage: function(message) {
		$.ajax({
			url: '/message',
			method: 'POST',
			contentType: 'application/json; charset=UTF-8',
			dataType: 'json',
			data: JSON.stringify(message),

			success: function() {
				this.getMessages({ init: false });
			}.bind(this),
		});
	},

	showMessage: function(index) {
		var currentIndex = props.public.messageBoxState.index;

		if (currentIndex === index) {
			currentIndex = -1;
		} else {
			currentIndex = index;
		}

		var shouldSetAsRead = currentIndex >= 0;
		var message = props.public.messageBoxState.messages[currentIndex];

		props.public.messageBoxState.index = currentIndex;
		this.emitChange();

		if (shouldSetAsRead) {
			process.nextTick(function() {
				this.setMessage({ id: message.message_id, read: 1 });
			}.bind(this));
		}
	},

	setRequestState: function(state) {
		var requestState = props.public.requestState;

		if (state.type !== undefined) {
			requestState.type = state.type;
		}
		if (state.description !== undefined) {
			requestState.description = state.description;
		}
		if (state.isLoading !== undefined) {
			requestState.isLoading = state.isLoading;
		}

		props.public.requestState = requestState;
		this.emitChange();
	},

	sendRequest: function() {
		var requestState = props.public.requestState;

		requestState.logMessage = '';
		props.public.requestState = requestState;
		this.emitChange();

		process.nextTick(function() {
			var message = {
				kind: requestState.type,
				content: requestState.description,
				notify: true
			};

			$.ajax({
				url: '/message',
				method: 'POST',
				contentType: 'application/json; charset=UTF-8',
				dataType: 'json',
				data: JSON.stringify(message),

				success: function() {
					console.log('success');
					requestState.type = 0;
					requestState.description = '';
					requestState.logMessageType = 0;
					requestState.logMessage = 'Successfully sent message!';
				},
				error: function() {
					console.log('error');
					requestState.logMessageType = 1;
					requestState.logMessage = 'Failed to send message to admin.';
				},
				complete: function() {
					requestState.isLoading = false;
					props.public.requestState = requestState;
					this.emitChange();
				}.bind(this)
			});
		}.bind(this));
	},

	getState: function() {
		return props.public;
	},

	emitChange: function() {
		this.emit(Constants.CHANGE);
	},

	addChangeListener: function(callback) {
		this.on(Constants.CHANGE, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(Constants.CHANGE, callback);
	}
});

Dispatcher.register(function(action) {
	if (action.actionType === Constants.ACTION.GET_VERSION) {
		Controller.getVersion();
	} else if (action.actionType === Constants.ACTION.GET_UPDATE_LOGS) {
		Controller.getUpdateLogs();
	} else if (action.actionType === Constants.ACTION.SEARCH) {
		Controller.search(action.searchText, action.option);
	} else if (action.actionType === Constants.ACTION.GET_FILTERS) {
		Controller.getFilters(action.callback);
	} else if (action.actionType === Constants.ACTION.SET_FILTER_INDEX) {
		Controller.setFilterIndex(action.filterIndex);
	} else if (action.actionType === Constants.ACTION.GET_TOKEN_FROM_SESSION) {
		Controller.getTokenFromSession();
	} else if (action.actionType === Constants.ACTION.POPUP_LOGIN_FORM) {
		Controller.popUpLogInForm(action.show);
	} else if (action.actionType === Constants.ACTION.OPEN_INSERT_FORM) {
		Controller.openInsertForm(action.show, action.option);
	} else if (action.actionType === Constants.ACTION.UPDATE_TERMS) {
		Controller.updateTerms(action.terms);
	} else if (action.actionType === Constants.ACTION.LOG_IN) {
		Controller.logIn(action.username, action.password);
	} else if (action.actionType === Constants.ACTION.LOG_OUT) {
		Controller.logOut(action.option);
	} else if (action.actionType === Constants.ACTION.UPLOAD_DATA) {
		Controller.uploadData(action.filename, action.data);
	} else if (action.actionType === Constants.ACTION.DELETE_ON_UPLOADABLE_TERMS) {
		Controller.deleteOnUploadableTerm(action.termIndex);
	} else if (action.actionType === Constants.ACTION.EMPTY_TERMS) {
		Controller.emptyTerms();
	} else if (action.actionType === Constants.ACTION.DISMISS_POPUP) {
		Controller.dismissPopup();
	} else if (action.actionType === Constants.ACTION.GET_MESSAGES) {
		Controller.getMessages();
	} else if (action.actionType === Constants.ACTION.SET_MESSAGE) {
		Controller.setMessage(action.message);
	} else if (action.actionType === Constants.ACTION.SHOW_MESSAGE) {
		Controller.showMessage(action.index);
	} else if (action.actionType === Constants.ACTION.SET_REQUEST_STATE) {
		Controller.setRequestState(action.state);
	} else if (action.actionType === Constants.ACTION.SEND_REQUEST) {
		Controller.sendRequest();
	}
});

module.exports = Controller;

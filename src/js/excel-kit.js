var path = require('path'),
	fs = require('fs'),
	Excel = require('exceljs'),
	XLSX = require('xlsx');

var Constants = require('./constants.js');

var rootDir = undefined;

var util = {
	parseRow: function(row, rowNumber) {
		console.log('[ExcelKit] attempt to parse row #' + rowNumber);

		return new Promise(function(resolve, reject) {
			var valid = true;
			var term = {};

			row.eachCell({ includeEmpty: true }, function(cell, colNumber) {
				if (!valid) {
					return null;
				}

				if (colNumber === 1) {
					var type = cell.value;
					if (cell.value === null) {
						type = 0;
					}

					type = parseInt(type);
					if (isNaN(type)) {
						type = 0;
					}

					term.type = type;
				} else if (colNumber === 2) {
					if (cell.value === null || typeof cell.value !== 'string' || cell.value.length === 0) {
						valid = false;
					} else {
						term.keyword = cell.value;
					}
				} else if (colNumber === 3) {
					if (cell.value === null || typeof cell.value !== 'string' || cell.value.length === 0) {
						valid = false;
					} else {
						term.title = cell.value;
					}
				} else if (colNumber === 4) {
					if (typeof cell.value !== 'string' || cell.value.length === 0) {
						term.description = null;
					} else {
						term.description = cell.value;
					}
				} else if (colNumber === 5) {
					if (cell.value === null || typeof cell.value !== 'string' || cell.value.length === 0) {
						term.tags = [];
					} else {
						var tags = [];
						cell.value.split(',').forEach(function(tag) {
							var trimmed = tag.trim();
							if (trimmed.length > 0) {
								tags.push({
									name: trimmed
								});
							}
						});
						term.tags = tags;
					}
				}
			});

			console.log('[ExcelKit] parsing row #' + rowNumber + ' ended with status [' + (valid ? 'SUCCESS' : 'FAILED') + ']');

			if (valid) {
				resolve(term);
			} else {
				resolve(null);
			}
		});
	},

	initParsing: function(file) {
		var promises = [];
		var fetching = true;

		return new Promise(function(resolve, reject) {
			if (file.type === Constants.EXCEL.TYPE.XLSX) {
				var workbook = new Excel.Workbook();

				workbook.xlsx.readFile(file.path).then(function() {
					workbook.eachSheet(function(worksheet, sheetId) {
						worksheet.eachRow(function(row, rowNumber) {
							fetching = true;
							promises.push(util.parseRow(row, rowNumber));
						});
					});
				});
			} else if (file.type === Constants.EXCEL.TYPE.XLS) {
				var workbook = XLSX.readFile(file.path);

				workbook.SheetNames.forEach(function(sheetName) {
					function passRow(row) {
						fetching = true;

						if (row.keyword === undefined || row.title === undefined) {
							return;
						}

						if (row.type === undefined) {
							row.type = 0;
						} else if (row.description === undefined) {
							row.description = null;
						} else if (row.tags === undefined) {
							row.tags = [];
						}

						promises.push(Promise.resolve(row));
					}

					var worksheet = workbook.Sheets[sheetName];

					var row = {};
					var rowNumber = 1;

					for (z in worksheet) {
						if(z[0] === '!') {
							continue;
						}

						var aRowNumber = parseInt(z.substring(1));

						if (aRowNumber !== rowNumber) {
							passRow(row);
							row = {};
							rowNumber = aRowNumber;
						}

						var columnIndex = z.substring(0, 1);
						var value = worksheet[z].v;

						if (columnIndex == 'A') {
							var type = parseInt(value);
							row.type = isNaN(type) ? 0 : type;
						} else if (columnIndex == 'B') {
							row.keyword = value;
						} else if (columnIndex == 'C') {
							row.title = value;
						} else if (columnIndex == 'D') {
							if (typeof value === 'string' && value.length > 0) {
								row.description = value;
							} else {
								row.description = null;
							}
						} else if (columnIndex == 'E') {
							row.tags = [];

							if (typeof value === 'string' && value.length > 0) {
								value.split(',').forEach(function(tag) {
									var trimmed = tag.trim();
									if (trimmed.length > 0) {
										row.tags.push({
											name: trimmed
										});
									}
								});
							}
						}
					}

					passRow(row);
				});
			} else if (file.type === Constants.EXCEL.TYPE.CSV) {
				var option = {
					map: function(value, index) {
						switch(index) {
							case 0:
								return parseInt(value);
							default:
								return value;
						}
					}
				};

				var workbook = new Excel.Workbook();

				workbook.csv.readFile(file.path, option).then(function(worksheet) {
					worksheet.eachRow(function(row, rowNumber) {
						fetching = true;
						promises.push(util.parseRow(row, rowNumber));
					});
				});
			}

			var runParsing = function() {
				setTimeout(function() {
					if (fetching) {
						fetching = false;

						process.nextTick(function() {
							runParsing();
						});
					} else {
						process.nextTick(function() {
							Promise.all(promises).then(function(_results) {
								var results = [];
								_results.forEach(function(result) {
									if (result !== null) {
										results.push(result);
									}
								})
								resolve(results);
							});
						});
					}
				}, 250);
			};

			runParsing();
		});
	}
};

var setRootDir = function(dir) {
	rootDir = dir;
	return this;
};

var clean = function() {
	console.log('[ExcelKit] cleaning temp dir');

	var tempDir = path.join(rootDir, '/temp');

	return new Promise(function(resolve, reject) {
		fs.readdir(tempDir, function(err, files) {
			if (err) {
				console.log('[ExcelKit] temp dir does not exist, should make one');
				fs.mkdirSync(tempDir);
			} else {
				files.forEach(function(filename) {
					console.log('[ExcelKit] cleaning: "' + filename + '"');
					fs.unlinkSync(path.join(tempDir, filename));
				});
			}

			resolve(tempDir);
		});
	});
};

var parse = function(file) {
	console.log('[ExcelKit] incoming request: ' + file.name);

	if (file.type !== Constants.EXCEL.TYPE.XLSX && file.type !== Constants.EXCEL.TYPE.XLS && file.type !== Constants.EXCEL.TYPE.CSV) {
		return Promise.reject('Invalid file type');
	}

	return new Promise(function(resolve, reject) {
		util.initParsing(file).then(function(results) {
			resolve(results);
		}, function(err) {
			reject(err);
		});
	});
};

module.exports.root = setRootDir;
module.exports.clean = clean;
module.exports.parse = parse;


var db = require('./database.js'),
	mapper = require('./mapper.js');

var Constants = require('./constants.js');

var util = {
	makeMatchQuery: function(match) {
		var target = match.target || Constants.SEARCH.TARGET.ALL;

		var matchQuery = '';

		if (target === Constants.SEARCH.TARGET.NORMAL) {
			matchQuery += 'keyword ilike ' + "'" + '%' + match.text + '%' + "'" + ' or ';
			matchQuery += 'title ilike ' + "'" + '%' + match.text + '%' + "'" + ' or ';
			matchQuery += 'description ilike ' + "'" + '%' + match.text + '%' + "'";
		} else if (target === Constants.SEARCH.TARGET.GROUP) {
			matchQuery += 'type_name ilike ' + "'" + '%' + match.text + '%' + "'";
		} else if (target === Constants.SEARCH.TARGET.TAG) {
			matchQuery += 'name ilike ' + "'" + '%' + match.text + '%' + "'";
		} else {
			matchQuery += 'true';
		}

		return matchQuery;
	},

	getInfoOfMatches: function(matches) {
		var info = {
			combinedMatches: [],
			tagMatches: [],
		};

		matches.forEach(function(match) {
			if (match.target === Constants.SEARCH.TARGET.NORMAL || match.target === Constants.SEARCH.TARGET.GROUP) {
				info.combinedMatches.push(match);
			} else if (match.target === Constants.SEARCH.TARGET.TAG) {
				info.tagMatches.push(match);
			} else {
				info.combinedMatches.push(match);
				info.tagMatches.push(match);
			}
		});

		return info;
	},

	makeQuery: function(page, matches, criteria) {
		var info = this.getInfoOfMatches(matches);

		var query = '';

		query += 'with combined as (select A.*, B.title as type_name from terms A left join filters B on A.type = B.filter_id), ';
		query += 'ids as ((select term_id from combined where ';

		if (info.combinedMatches.length > 0) {
			info.combinedMatches.forEach(function(match, index) {
				query += '(' + this.makeMatchQuery(match) + ')';
				if (index < info.combinedMatches.length - 1) {
					query += ' and ';
				}
			}.bind(this));
		} else {
			query += 'false';
		}

		query += ')';

		if (criteria === Constants.SEARCH.CRITERIA.OR) {
			query += ' union ';
		} else {
			query += ' intersect ';
		}

		query += '(select term_id from tags where ';

		if (info.tagMatches.length > 0) {
			info.tagMatches.forEach(function(match, index) {
				query += '(' + this.makeMatchQuery(match) + ')';
				if (index < info.tagMatches.length - 1) {
					query += ' and ';
				}
			}.bind(this));
		} else {
			query += 'false';
		}

		query += ')), ';
		query += 'matches as (select C.* from terms C, ids D where C.deleted = 0 and C.term_id = D.term_id) ';

		query += 'select E.*, F.tag_id, F.name as tag_name, F.deleted as tag_deleted, F.created_at as tag_created_at, F.updated_at as tag_updated_at, G.title as type_name, G.color as type_color ';
		query += 'from matches E ';
		query += 'left join tags F on (F.deleted = 0 and E.term_id = F.term_id) ';
		query += 'left join filters G on (E.type = G.filter_id) ';
		query += 'order by E.updated_at desc, tag_updated_at desc, term_id, tag_id';

		query += ' offset ' + page * Constants.SEARCH.LIMIT;
		query += ' limit ' + Constants.SEARCH.LIMIT;

		return query;
	},

	highlightResults: function(results, matches) {
		function highlight(target, text) {
			
			
			if (target === undefined || target === null || target.length === 0) {
				return target;
			}

			var index = target.toLowerCase().search(text.toLowerCase());
			if (index === -1) {
				return target;
			}

			var length = text.length;

			var preTag = Constants.SEARCH.TAG.PRE;
			var postTag = Constants.SEARCH.TAG.POST;

			var head = target.substring(0, index);
			var body = target.substring(index, index + length);
			var tail = target.substring(index + length, target.length);

			return '' + head + preTag + body + postTag + tail;
		};

		for (var i = 0; i < matches.length; i++) {
			var match = matches[i];
			if (match.target === Constants.SEARCH.TARGET.ALL) {
				continue;
			}

			results.forEach(function(term) {
				if (match.target === Constants.SEARCH.TARGET.NORMAL) {
					term.keyword = highlight(term.keyword, match.text);
					term.title = highlight(term.title, match.text);
					term.description = highlight(term.description, match.text);
				}
			});
		}	

		return results;
	}
};

var run = function(_page, _matches, _criteria) {
	var page = 0;
	var matches = [];
	var criteria = Constants.SEARCH.CRITERIA.OR;

	if (_page !== undefined) {
		page = _page || page;
		page = parseInt(page);

		if (isNaN(page)) {
			page = 0;
		}
	}

	if (_matches === undefined || _matches === null) {
		matches.push({ target: Constants.SEARCH.TARGET.ALL });
	} else if (_matches instanceof Array) {
		matches = _matches;
	} else if (_matches instanceof Object) {
		matches.push(_matches);
	} else {
		console.log('[SearchKit] unspecified operations');
		return [];
	}

	if (_criteria !== undefined) {
		criteria = _criteria || criteria;
	}

	console.log('[SearchKit] perform search with parameters:');
	console.log('[SearchKit] page = ' + page);

	var matchesLog = '[';
	matches.forEach(function(match, index) {
		matchesLog += '{';
		matchesLog += 'target: ' + match.target + ', ';
		matchesLog += 'text: ' + match.text;
		matchesLog += '}';
		if (index < matches.length - 1) {
			matchesLog += ', ';
		}
	});
	matchesLog += ']';
	console.log('[SearchKit] matches = ' + matchesLog);

	console.log('[SearchKit] criteria = ' + criteria);

	return new Promise(function(resolve, reject) {
		var query = util.makeQuery(page, matches, criteria);

		db.run(query).then(function(rows) {
			var mapped = mapper.map(rows);
			var highlighted = util.highlightResults(mapped, matches);
			resolve(highlighted);
		}, function(err) {
			reject(err);
		});
	});
};

module.exports.run = run;


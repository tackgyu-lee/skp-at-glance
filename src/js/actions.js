var Dispatcher = require('./dispatcher.js'),
	Constants = require('./constants.js');

var Actions = {
	getVersion: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.GET_VERSION
		});
	},

	getUpdateLogs: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.GET_UPDATE_LOGS
		});
	},

	search: function(searchText, option) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.SEARCH,
			searchText: searchText,
			option: option
		});
	},

	getFilters: function(callback) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.GET_FILTERS,
			callback: callback
		});
	},

	setFilterIndex: function(filterIndex) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.SET_FILTER_INDEX,
			filterIndex: filterIndex
		});
	},

	getTokenFromSession: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.GET_TOKEN_FROM_SESSION
		});
	},

	popUpLogInForm: function(show) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.POPUP_LOGIN_FORM,
			show: show
		});
	},

	openInsertForm: function(show, option) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.OPEN_INSERT_FORM,
			show: show,
			option: option
		});
	},

	updateTerms: function(terms) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.UPDATE_TERMS,
			terms: terms
		});
	},

	deleteTerm: function(term) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.DELETE_TERM,
			term: term
		});
	},

	logIn: function(username, password) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.LOG_IN,
			username: username,
			password: password
		});
	},

	logOut: function(option) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.LOG_OUT,
			option: option
		});
	},

	uploadData: function(filename, data) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.UPLOAD_DATA,
			filename: filename,
			data: data
		});
	},

	deleteOnUploadableTerm: function(termIndex) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.DELETE_ON_UPLOADABLE_TERMS,
			termIndex: termIndex
		});
	},

	emptyTerms: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.EMPTY_TERMS
		});
	},

	dismissPopup: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.DISMISS_POPUP
		})
	},

	getMessages: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.GET_MESSAGES
		});
	},

	setMessage: function(message) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.SET_MESSAGE,
			message: message
		});
	},

	showMessage: function(index) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.SHOW_MESSAGE,
			index: index
		});
	},

	setRequestState: function(state) {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.SET_REQUEST_STATE,
			state: state
		});
	},

	sendRequest: function() {
		Dispatcher.dispatch({
			actionType: Constants.ACTION.SEND_REQUEST
		});
	}
};

module.exports = Actions;
var moment = require('moment');

var convertDate = function(date) {
	if (date === undefined || date === null) {
		return null;
	}

	var m = moment(date);
	var utcOffset = m.utcOffset();
	m.subtract(utcOffset, 'minutes');
	var str = m.format('YYYY-MM-DD HH:mm:ss.SSSSSS');
	return str.substr(0, str.length-3);
};

var mapTerms = function(items) {
	var results = [];

	var getTerm = function(item) {
		return {
			id: item.term_id,
			createdAt: item.created_at,
			updatedAt: item.updated_at,
			deleted: item.deleted !== undefined && item.deleted === true,

			type: item.type,
			keyword: item.keyword,
			title: item.title,
			description: item.description,

			tags: [],

			typeName: item.type_name,
			typeColor: item.type_color
		};
	};

	var getTag = function(item) {
		return item.tag_id === undefined || item.tag_id === null ? null : {
			id: item.tag_id,
			createdAt: item.tag_created_at,
			updatedAt: item.tag_updated_at,
			deleted: item.tag_deleted !== undefined && item.tag_deleted === true,

			name: item.tag_name
		};
	};

	var length = items.length;
	var count = 0;

	for (var i = 0; i < length; i++) {
		var item = items[i];

		if (count == 0 || item.term_id !== results[count-1].id) {
			results.push(getTerm(item));
			count++;
		}

		var tag = getTag(item);

		if (tag !== null) {
			results[count-1].tags.push(tag);
		}
	}

	return results;
};

var unmapTerms = function(_items) {
	var items;

	if (_items instanceof Array) {
		items = _items;
	} else {
		items = [_items];
	}

	var results = [];

	var getTerm = function(item) {
		return  {
			term_id: item.id,
			created_at: convertDate(item.createdAt),
			updated_at: convertDate(item.updatedAt),
			deleted: item.deleted === true ? 1 : 0,

			type: item.type,
			keyword: item.keyword,
			title: item.title,
			description: item.description
		};
	};

	var getTag = function(item) {
		return {
			tag_id: item.id,
			created_at: convertDate(item.createdAt),
			updated_at: convertDate(item.updatedAt),
			tag_deleted: item.deleted === true ? 1 : 0,

			name: item.name
		};
	}

	var length = items.length;

	for (var i = 0; i < length; i++) {
		var itemTerm = items[i];

		var result = {
			term: getTerm(itemTerm),
			tags: []
		};

		if (itemTerm.tags !== undefined && itemTerm.tags !== null && itemTerm.tags.length > 0) {
			var tagCount = itemTerm.tags.length;

			for (var j = 0; j < tagCount; j++) {
				var itemTag = itemTerm.tags[j];
				result.tags.push(getTag(itemTag));
			}
		}

		results.push(result);
	}

	return results;
};

module.exports.convertDate = convertDate;

module.exports.map = mapTerms;
module.exports.unmap = unmapTerms;
var jwt = require('jsonwebtoken');

var Constants = require('./constants.js');

var generate = function(secret, user) {
	console.log('[SessionKit] generate token');

	return jwt.sign({
		target: user
	}, secret, {
		expiresIn: Constants.SERVER.TOKEN_LIFETIME
	})
};

var verify = function(secret, token) {
	console.log('[SessionKit] verify token');

	if (token === null) {
		console.log('[SessionKit] token not provided');
		return Promise.reject('Token not provided');
	} else if (secret === null) {
		console.log('[SessionKit] secret not provided');
		return Promise.reject('Secret not provided');
	}

	return new Promise(function(resolve, reject) {
		jwt.verify(token, secret, function(err, decoded) {
			if (err) {
				console.log('[SessionKit] invalid token');
				reject('Invalid token');
			} else {
				console.log('[SessionKit] verified');
				resolve(decoded);
			}
		});
	});
};

module.exports.generate = generate;
module.exports.verify = verify;


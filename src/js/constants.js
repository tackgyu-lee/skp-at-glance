module.exports.CHANGE = 'CHANGE';

module.exports.CALLER = {
	APP: 0,
	UPLOAD: 1,
	MESSAGE: 2,
	UPDATE: 3,
	REQUEST: 4
};

module.exports.ACTION = {
	GET_VERSION: 0,
	GET_UPDATE_LOGS: 1,
	SEARCH: 2,
	GET_FILTERS: 3,
	SET_FILTER_INDEX: 4,
	GET_TOKEN_FROM_SESSION: 5,
	POPUP_LOGIN_FORM: 6,
	OPEN_INSERT_FORM: 7,
	UPDATE_TERMS: 8,
	LOG_IN: 9,
	LOG_OUT: 10,
	UPLOAD_DATA: 11,
	DELETE_ON_UPLOADABLE_TERMS: 12,
	EMPTY_TERMS: 13,
	DISMISS_POPUP: 14,
	GET_MESSAGES: 15,
	SET_MESSAGE: 16,
	SHOW_MESSAGE: 17,
	SET_REQUEST_STATE: 18,
	SEND_REQUEST: 19
};

module.exports.SERVER = {
	PORT_NUM: process.env.PORT || 8000,
	TOKEN_LIFETIME: '1d',
	MAX_RETRY: 3,
	RETRY_INTERVAL: 10
};

module.exports.FILTER = {
	FAILED: -1,
	INIT: 0,
	LOADED: 1
};

module.exports.SEARCH = {
	LIMIT: 50,
	TARGET: {
		ALL: 0,
		NORMAL: 1,
		GROUP: 2,
		TAG: 3
	},
	CRITERIA: {
		AND: 0,
		OR: 1
	},
	TAG: {
		PRE: '<em>',
		POST: '</em>'
	}
}

module.exports.EXCEL = {
	TYPE: {
		XLSX: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
		XLS: 'application/vnd.ms-excel',
		CSV: 'text/csv',
	}
}

module.exports.SLACK_HOOK_URL = 'https://hooks.slack.com/services/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';

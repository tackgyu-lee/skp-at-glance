var React = require('react'),
	ReactDOM = require('react-dom');

var Uploader = require('./jsx/uploader.jsx');

ReactDOM.render(
	<Uploader />, document.getElementById('uploader')
);


var React = require('react'),
	ReactDOM = require('react-dom');

var RequestForm = require('./jsx/request-form.jsx');

ReactDOM.render(
	<RequestForm />, document.getElementById('request_form')
);


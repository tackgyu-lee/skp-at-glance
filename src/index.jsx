var React = require('react'),
	ReactDOM = require('react-dom');

var App = require('./jsx/app.jsx');

ReactDOM.render(
	<App />, document.getElementById('app')
);


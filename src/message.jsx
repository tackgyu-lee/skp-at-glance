var React = require('react'),
	ReactDOM = require('react-dom');

var MessageBox = require('./jsx/message-box.jsx');

ReactDOM.render(
	<MessageBox />, document.getElementById('message_box')
);


var React = require('react'),
	ReactDOM = require('react-dom');

var Controller = require('../js/controller.js'),
	Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var Header = require('./header.jsx'),
	SearchBar = require('./search-bar.jsx'),
	FilterDropdown = require('./filter-dropdown.jsx'),
	SearchList = require('./search-list.jsx'),
	Footer = require('./footer.jsx'),
	LogInForm = require('./login-form.jsx'),
	InsertForm = require('./insert-form.jsx');

var styles = {
	popUpGrid: {
		height: '100%'
	},
	popUpColumn: {
		maxWidth: '350px'
	},
	container: {
		marginTop: '65px',
		marginBottom: '60px'
	},
	statusBar: {
		paddingTop: '0px',
		paddingBottom: '0px',
		marginTop: '-15px',
		marginBottom: '-26px'
	},
	listColumn: {
		marginTop: '10px'
	},
	insertFormColumn: {
		paddingTop: '0px',
		paddingBottom: '0px'
	},
	listFooter: {
		height: '60px',
		marginTop: '60px',
		marginBottom: '60px'
	},
	notification: {
		backgroundColor: 'rgba(1, 1, 1, 0.5)',
		height: '50px'
	}
};

var App = React.createClass({
	getInitialState: function() {
	    return Controller.getState();
	},

	onChange: function() {
		this.setState(Controller.getState());

		$(ReactDOM.findDOMNode(this.refs.logInForm))
			.dimmer(this.state.logInFormState.shouldPopUp === true ? 'show' : 'hide');
	},

	onClickRefresh: function() {
		Actions.search(this.state.searchText, { append: true });
	},

	componentDidMount: function() {
		Controller.addChangeListener(this.onChange);

		Actions.search('');
		Actions.getVersion();
		Actions.getFilters();
		Actions.getTokenFromSession();
		Actions.getMessages();

		$(window).scroll(function() {
			if (!this.state.isInitiated || !this.state.hasMore) {
				return;
			}

			if (window.pageYOffset + $(window).height() >= $('#app').height()) {
				Actions.search(this.state.searchText, { append: true, fixScroll: true });
			}
		}.bind(this));
	},

	componentDidUpdate: function() {
		if (this.state.shouldScrollToTop) {
	        $('html,body').animate({ scrollTop: 0 }, 300);
		}
	},

	componentWillUnmount: function() {
		Controller.removeChangeListener(this.onChange);
	},

	render: function() {
		var hasMore = this.state.hasMore;
		var isSearching = this.state.isSearching;

		return (
			<div ref="container">
				<div ref="logInForm" className="ui page dimmer">
					<div className="content">
						<div className="ui middle aligned center aligned grid" style={styles.popUpGrid}>
							<div className="column" style={styles.popUpColumn}>
								<LogInForm logInFormState={this.state.logInFormState}/>
							</div>
						</div>
					</div>
				</div>

				<Header isFetchingToken={this.state.isFetchingToken}
					token={this.state.token}
					insertFormState={this.state.insertFormState}
					shouldDismissPopup={this.state.shouldDismissPopup}
					messages={this.state.messageBoxState.messages}
					caller={Constants.CALLER.APP} />

				<div className="ui container" style={styles.container}>
					<div className="ui fluid grid">
						<div className="sixteen wide column" style={styles.insertFormColumn}>
							<InsertForm insertFormState={this.state.insertFormState} filterState={this.state.filterState}/>
						</div>
						
						<div className="sixteen wide column">
							<SearchBar searchText={this.state.searchText} isSearching={this.state.isSearching}/>
						</div>

						<div className="middle aligned row" style={styles.statusBar}>
							<div className="eight wide column">
								<FilterDropdown filterState={this.state.filterState}/>
							</div>

							<div className="eight wide right aligned column">
								<div className={'ui horizontal mini statistic ' + (this.state.terms.length > 0 ? 'blue' : 'red')}>
									<div className="value">
										{this.state.terms.length}
									</div>
									<div className="label">
										results
									</div>
								</div>
							</div>
						</div>

						<div className="sixteen wide column" style={styles.listColumn}>
							<SearchList terms={this.state.terms} token={this.state.token} insertFormState={this.state.insertFormState}/>
						</div>
					</div>
				</div>

				{!hasMore && !isSearching ? null :
					<div className={'ui basic center aligned segment' + (isSearching ? ' loading' : '')} style={styles.listFooter}>
						{!hasMore ? null :
							<button className="ui basic button" onClick={this.onClickRefresh}>load more</button>
						}
					</div>
				}

				<Footer version={this.state.version}/>
			</div>
		);
	}
});

module.exports = App;

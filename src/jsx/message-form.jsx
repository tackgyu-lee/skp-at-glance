var React = require('react');

var styles = {
	content: {
		paddingLeft: '5px',
		paddingRight: '5px'
	},
	title: {
		marginRight: '4px',
		fontSize: '1.1em',
	    fontWeight: 'bold'
	}
};

var MessageForm = React.createClass({
	render: function() {
		var isActive = this.props.isActive;
		var message = this.props.message;

		var description = [];
		var splitted = message.content.split('\n');
		
		splitted.forEach(function(line, index) {
			var trimmed = line.trim();

			description.push(trimmed.length > 0 ? trimmed : '');

			if (index < splitted.length - 1) {
				description.push(<br/>)
			}
		});

		var title = '';

		if (message.kind === 1) {
			title += 'new keyword suggestion';
		} else {
			title += 'general proposal';
		}

		return (
			<div ref="container">
				<div className="ui accordion">
					<div className={'title' + (isActive ? ' active' : '')} onClick={this.props.onClick}>
						<i className="dropdown icon"></i>
						<span style={styles.title}>{title}</span>
						{message.read === 0 ?
							<a className="ui red empty circular label"></a>
							: null
						}
					</div>

					<div className={'content' + (isActive ? ' active' : '')} style={styles.content}>
						<p className="transition visible">{description}</p>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = MessageForm;

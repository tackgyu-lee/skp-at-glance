var React = require('react'),
	moment = require('moment');

var FilterCheckbox = require('./filter-checkbox.js');

var Actions = require('../js/actions.js');

var styles = {
	segment: {
		paddingLeft: '0px',
		paddingRight: '0px'
	},
	inputFieldColumn: {
		paddingBottom: '0px'
	},
	buttonRow: {
		paddingTop: '0px',
		paddingBottom: '0px'
	},
	titleField: {
		paddingLeft: '0px'
	},
	form: {
		marginBottom: '30px'
	},
	textarea: {
		resize: 'none'
	},
	tagColumn: {
		paddingTop: '0px',
		paddingBottom: '0px'
	},
	tag: {
		paddingLeft: '3px',
		paddingRight: '3px',
		border: '0px'
	},
	tagDeleted: {
		paddingLeft: '3px',
		paddingRight: '3px',
		border: '0px',
		opacity: '0.5'
	},
	tagField: {
		marginBottom: '6px'
	},
	tagHelp: {
		textAlign: 'center',
		color: '#767676',
		fontSize: '0.9em'
	}
};

var InsertForm = React.createClass({
	getInitialState: function() {
		return {
			type: 0,
			keyword: '',
			title: '',
			description: '',
			tag: '',

			tags: [],

			lastIndex: 0,
			errorIndex: 0,
			isLoading: false,
			canSubmit: false
		};
	},

	didChange: function() {
		var term = this.props.insertFormState.term;

		var type = this.state.type;
		var keyword = this.state.keyword;
		var title = this.state.title;
		var description = this.state.description;
		var tags = this.state.tags;

		if (term === null) {
			return keyword.length > 0 && title.length > 0;
		}

		if (type !== term.type || keyword !== term.keyword || title !== term.title) {
			return true;
		}

		if (term.description === null && description.length > 0) {
			return true;
		} else if (term.description !== null && description !== term.description) {
			return true;
		}

		if (tags.length !== term.tags.length) {
			return true;
		}

		for (var i = 0; i < tags.length; i++) {
			if (tags[i].name !== term.tags[i].name || tags[i].deleted !== term.tags[i].deleted) {
				return true;
			}
		}

		return false;
	},

	setSubmitButtonState: function() {
		process.nextTick(function() {
			this.setState({ canSubmit: this.didChange() });
		}.bind(this));
	},

	onFocus: function(index, event) {
		this.setState({ lastIndex: index });
	},

	onChange: function(index, event) {
		var text = event.target.value;

		if (index === 0) {
			this.setState({ keyword: text });
		} else if (index === 1) {
			this.setState({ title: text });
		} else if (index === 2) {
			this.setState({ description: text });
		} else if (index === 3) {
			this.setState({ tag: text });
		}

		this.setSubmitButtonState();
	},

	onChangeType: function(index) {
		this.setState({ type: index });
		this.setSubmitButtonState();
	},

	onKeyPress: function(index, event) {
		if (event.key !== 'Enter') {
			return;
		}

		var inputIndex = index;
		var inputText = event.target.value;

		this.setState({ errorIndex: 0 });

		process.nextTick(function() {
			if (inputIndex === 0) {
				if (this.state.keyword.length === 0) {
					this.setState({ errorIndex: 1 });
				} else if (this.state.title.length === 0) {
					this.setState({ errorIndex: 2 });
					this.refs.title.focus();
				} else {
					this.refs.description.focus();
				}
			} else if (inputIndex === 1) {
				if (this.state.title.length === 0) {
					this.setState({ errorIndex: 2 });
				} else if (this.state.keyword.length === 0) {
					this.refs.keyword.focus();
					this.setState({ errorIndex: 1 });
				} else {
					this.refs.description.focus();
				}
			} else if (inputIndex === 2) {
				this.refs.tag.focus();
			} else if (inputIndex === 3 && inputText.length > 0) {
				var tags = this.state.tags;

				tags.splice(0, 0, {
					name: inputText
				});

				this.setState({ tag: '', tags: tags });
			}

			this.setSubmitButtonState();
		}.bind(this));
	},

	onClick: function(index, event) {
		if (index === 0) {
			Actions.openInsertForm(false);
		} else if (index === 1) {
			this.refs.keyword.blur();
			this.refs.title.blur();
			this.refs.description.blur();
			this.refs.tag.blur();

			var term = this.props.insertFormState.term;

			if (term === null) {
				term = {};
			}

			term.type = this.state.type;
			term.keyword = this.state.keyword;
			term.title = this.state.title;
			term.description = this.state.description;
			term.tags = this.state.tags;

			this.setState({ isLoading: true });

			process.nextTick(function() {
				Actions.updateTerms([term]);
			});
		}
	},

	onClickTag: function(index, event) {
		var tags = this.state.tags;

		if (tags[index].id === undefined) {
			tags.splice(index, 1);
		} else {
			if (tags[index].deleted === undefined || tags[index].deleted === false) {
				tags[index].deleted = true;
			} else {
				tags[index].deleted = false;
			}
		}

		this.setState({ tags: tags });
		this.setSubmitButtonState();
	},

	componentWillReceiveProps: function(nextProps) {
		var lastIndex = this.state.lastIndex;
		var type = 0;
		var keyword = '';
		var title = '';
		var description = '';
		var tag = '';
		var tags = [];

		if (nextProps.insertFormState.term !== null) {
			lastIndex = 0;

			type = nextProps.insertFormState.term.type;
			keyword = nextProps.insertFormState.term.keyword;
			title = nextProps.insertFormState.term.title;

			if (nextProps.insertFormState.term.description !== null) {
				description = nextProps.insertFormState.term.description;
			}

			nextProps.insertFormState.term.tags.forEach(function(aTag) {
				tags.push({
					id: aTag.id,
					createdAt: aTag.createdAt,
					updatedAt: aTag.updatedAt,
					deleted: aTag.deleted,
					name: aTag.name
				});
			});
		} else {
			tags = [];
		}

		this.setState({
			type: type,
			keyword: keyword,
			title: title,
			description: description,
			tag: tag,

			tags: tags,

			lastIndex: lastIndex,
			isLoading: false
		});

		this.setSubmitButtonState();
	},

	render: function() {
		var insertFormState = this.props.insertFormState;

		if (insertFormState.isOpen !== true) {
			return null;
		}

		var keyword = this.state.keyword;
		var title = this.state.title;
		var description = this.state.description;
		var tag = this.state.tag;
		var tags = this.state.tags;

		return (
			<div className={'ui basic segment' + (this.state.isLoading ? ' loading' : '')} style={styles.segment}>

				<form className="ui form" style={styles.form}>
					<div className="ui fluid grid">

						<div className="ui row" style={styles.inputFieldColumn}>
							<div className="six wide column">
								<div className={'required field' + (this.state.errorIndex === 1 ? ' error' : '')}>
									<label>Keyword</label>
									<input ref="keyword" type="text" name="keyword" placeholder="required" value={keyword} onFocus={this.onFocus.bind(null, 0)} onChange={this.onChange.bind(null, 0)} onKeyPress={this.onKeyPress.bind(null, 0)}/>
								</div>
							</div>

							<div className="ten wide column" style={styles.titleField}>
								<div className={'required field' + (this.state.errorIndex === 2 ? ' error' : '')}>
									<label>Title</label>
									<input ref="title" type="text" name="title" placeholder="required" value={title} onFocus={this.onFocus.bind(null, 1)} onChange={this.onChange.bind(null, 1)} onKeyPress={this.onKeyPress.bind(null, 1)}/>
								</div>
							</div>
						</div>

						<div className="sixteen wide column" style={styles.inputFieldColumn}>
							<div className="field">
								<label>Description</label>
								<textarea ref="description" rows="4" placeholder="optional" value={description} onFocus={this.onFocus.bind(null, 2)} onChange={this.onChange.bind(null, 2)} onKeyPress={this.onKeyPress.bind(null, 2)} style={styles.textarea}></textarea>
							</div>
						</div>

						<div className="sixteen wide column" style={styles.inputFieldColumn}>
							<FilterCheckbox ref="filterCheckbox" filterState={this.props.filterState} index={this.state.type} onChange={this.onChangeType}/>
						</div>

						<div className="sixteen wide column" style={styles.tagColumn}>
							<div className="field" style={styles.tagField}>
								<label>Tag</label>
								<input ref="tag" type="text" name="description" placeholder="optional, press enter to insert tag" value={tag} onFocus={this.onFocus.bind(null, 3)} onChange={this.onChange.bind(null, 3)} onKeyPress={this.onKeyPress.bind(null, 3)}/>
							</div>

							{tags.length === 0 ? null :
								<div className="sixteen wide column">
									{tags.map(function(tag, index) {
										if (tag.deleted === true) {
											return (
												<a className="ui grey basic label" key={'tag-' + index} style={styles.tagDeleted} onClick={this.onClickTag.bind(null, index)}>{'# ' + tag.name}</a>
											);
										}

										return (
											<a className="ui blue basic label" key={'tag-' + index} style={styles.tag} onClick={this.onClickTag.bind(null, index)}>{'# ' + tag.name}</a>
										);
									}.bind(this))}

									<p style={styles.tagHelp}>Tap any tag to remove from update.</p>
								</div>
							}
						</div>

					</div>
				</form>

				<div className="ui grid">
					<div className="ui center aligned two column row" style={styles.buttonRow}>
						<div className="column">
							<button className="ui basic fluid button" onClick={this.onClick.bind(null, 0)}>Cancel</button>
						</div>

						<div className="column">
							<button className={'ui primary fluid button' + (this.state.canSubmit ? '' : ' disabled')} onClick={this.onClick.bind(null, 1)}>Submit</button>
						</div>
					</div>

					<div className="sixteen wide column" style={styles.inputFieldColumn}>
						<div className="ui divider"></div>
					</div>
				</div>

			</div>
		);
	}
});

module.exports = InsertForm;



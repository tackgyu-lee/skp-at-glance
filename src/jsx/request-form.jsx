var React = require('react'),
	ReactDOM = require('react-dom');

var Controller = require('../js/controller.js'),
	Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var Header = require('./header.jsx'),
	Footer = require('./footer.jsx'),
	LogInForm = require('./login-form.jsx'),
	MessageForm = require('./message-form.jsx');

var styles = {
	popUpGrid: {
		height: '100%'
	},
	popUpColumn: {
		maxWidth: '350px'
	},
	container: {
		marginTop: '65px',
		marginBottom: '32px',
	},
	segment: {
		padding: '0px'
	},
	header: {
		color: '#767676'
	},
	description: {
		marginTop: '30px'
	},
	textarea: {
		resize: 'none'
	}
};

var MessageBox = React.createClass({
	getInitialState: function() {
	    return Controller.getState();
	},

	onChange: function() {
		this.setState(Controller.getState());

		if (this.state.token !== null) {
			location.href = '/';
		} else {
			$(ReactDOM.findDOMNode(this.refs.logInForm))
				.dimmer(this.state.logInFormState.shouldPopUp === true ? 'show' : 'hide');
		}
	},

	onChangeDummy: function() {
		// dummy onChange
	},

	onChangeType: function(index, event) {
		Actions.setRequestState({
			type: index
		});
	},

	onChangeDescription: function(event) {
		Actions.setRequestState({
			description: event.target.value
		});
	},

	onClickSubmit: function() {
		var requestState = this.state.requestState;

		if (requestState.isLoading || requestState.description.length === 0) {
			return;
		}

		this.refs.description.blur();

		Actions.setRequestState({
			isLoading: true
		});

		process.nextTick(function() {
			Actions.sendRequest();
		});
	},

	componentDidMount: function() {
		Controller.addChangeListener(this.onChange);

		Actions.getVersion();
		Actions.getTokenFromSession();
	},

	componentWillUnmount: function() {
		Controller.removeChangeListener(this.onChange);
	},

	render: function() {
		var requestState = this.state.requestState;

		var segmentClassName = 'ui fluid basic segment';
		if (requestState.isLoading) {
			segmentClassName += ' loading';
		}

		var buttonClassName = 'ui primary button';
		if (requestState.isLoading || requestState.description.length === 0) {
			buttonClassName += ' disabled';
		}

		return (
			<div ref="container">
				<div ref="logInForm" className="ui page dimmer">
					<div className="content">
						<div className="ui middle aligned center aligned grid" style={styles.popUpGrid}>
							<div className="column" style={styles.popUpColumn}>
								<LogInForm logInFormState={this.state.logInFormState}/>
							</div>
						</div>
					</div>
				</div>

				<Header isFetchingToken={this.state.isFetchingToken}
					token={this.state.token}
					shouldRedirect={true}
					caller={Constants.CALLER.REQUEST} />

				<div className="ui container">
					<div className="ui fluid grid" style={styles.container}>

						<div className="sixteen wide column">

							<div className={segmentClassName} style={styles.segment}>

								<div className="ui form">
									<h3 style={styles.header}>Request type:</h3>

									<div className="grouped fields">

										<div className="field">
											<div className="ui radio checkbox" onClick={this.onChangeType.bind(null, 0)}>
												<input type="radio" checked={requestState.type === 0 ? 'checked' : null} tabindex="0" className="hidden" onChange={this.onChangeDummy}/>
												<label>general proposal</label>
											</div>
										</div>

										<div className="field">
											<div className="ui radio checkbox" onClick={this.onChangeType.bind(null, 1)}>
												<input type="radio" checked={requestState.type === 1 ? 'checked' : null} tabindex="0" className="hidden" onChange={this.onChangeDummy}/>
												<label>new keyword suggestion</label>
											</div>
										</div>

									</div>

									<h3 style={styles.header}>Request description:</h3>

									<textarea ref="description" rows="5" placeholder="request description" value={this.state.requestState.description} style={styles.textarea} onChange={this.onChangeDescription}></textarea>
								</div>

							</div>

						</div>

						<div className="sixteen wide center aligned column">
							<button className={buttonClassName} onClick={this.onClickSubmit}>Submit</button>
						</div>

						{requestState.logMessage.length > 0 ?
							<div className="sixteen wide center aligned column">
								<h4 className={'ui header' + (requestState.logMessageType === 1 ? ' red' : ' blue')}>{requestState.logMessage}</h4>
							</div>
							: null
						}

					</div>
				</div>

				<Footer version={this.state.version}/>
			</div>
		);
	}
});

module.exports = MessageBox;

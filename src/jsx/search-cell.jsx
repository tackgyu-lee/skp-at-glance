var React = require('react');

var Actions = require('../js/actions.js');

var SearchCellText = require('./search-cell-text.jsx');

var styles = {
	segment: {
		paddingLeft: '3px',
		paddingRight: '3px'
	},

	keyword: {
		color: 'grey',
		marginRight: '8px'
	},
	title: {
		marginRight: '12px'
	},

	filter: {
		border: '0px',
		marginLeft: '-4px'
	},
	tag: {
		border: '0px',
		marginLeft: '-10px'
	},

	modifyIcon: {
		marginLeft: '10px'
	},
	deleteIcon: {
		marginLeft: '-5px'
	},
	loadingIcon: {
		marginLeft: '-5px'
	},

	divider: {
		marginBottom: '2px'
	}
};

var SearchCell = React.createClass({
	onClickText: function(event) {
		var searchText = event.target.parentNode.innerText;

		if (searchText.search('@') === 0) {
			searchText = '@' + searchText.substr(2, searchText.length-2);
		} else if (searchText.search('#') === 0) {
			searchText = '#' + searchText.substr(2, searchText.length-2);
		}

		Actions.search(searchText);
	},

	onClickButton: function(index, event) {
		if (index === 0) {
			Actions.openInsertForm(true, {
				term: this.props.term
			});
		} else if (index === 1 && !this.props.onClickDelete) {
			var term = {
				deleted: true,

				id: this.props.term.id,

				type: this.props.term.type,
				keyword: this.props.term.keyword,
				title: this.props.term.title,
				description: this.props.term.description,

				tags: []
			};

			if (this.props.term.tags.length > 0) {
				this.props.term.tags.forEach(function(tag) {
					term.tags.push({
						id: tag.id,	
						name: tag.name,
						deleted: true
					});
				});
			}

			Actions.updateTerms([term]);
		} else if (index === 1 && this.props.onClickDelete) {
			var termIndex = parseInt(this.props.index);
			termIndex = isNaN(termIndex) ? -1 : termIndex;
			this.props.onClickDelete(termIndex);
		}
	},

	render: function() {
		var term = this.props.term;
		var token = this.props.token;
		var insertFormState = this.props.insertFormState;

		var description = [];

		if (term.description !== null && term.description.length > 0) {
			var comps = term.description.split('\n');
			comps.forEach(function(comp, index) {
				description.push(comp);
			});
		}

		var hasControl = false;
		var hasLoadingIndicator = false;

		if (token !== null && insertFormState !== undefined && !insertFormState.isOpen) {
			hasControl = !insertFormState.isModifying;
			hasLoadingIndicator = insertFormState.isModifying;
		}

		return (
			<div className="ui basic left aligned segment" style={styles.segment}>
				<div className="ui middle aligned fluid grid">
					<div className="sixteen wide column">
						<div className="ui medium header">
							<span style={styles.keyword}>
								<SearchCellText text={term.keyword}/>
							</span>

							<span style={styles.title}>
								<SearchCellText text={term.title}/>
							</span>

							{term.type === 0 ? null :
								<a className="ui basic label" style={styles.filter}>
									<SearchCellText
										text={'@ ' + term.typeName}
										color={term.typeColor}
										onClick={this.onClickText}/>
								</a>
							}

							{term.tags !== undefined && term.tags !== null && term.tags.length > 0 ?
								term.tags.map(function(tag, index) {
									if (tag.deleted === true) {
										return null;
									}

									return (
										<a className="ui basic label" key={'tag-' + index} style={styles.tag}>
											<SearchCellText
												text={'# ' + tag.name}
												color='rgba(41, 135, 205, 1.0)'
												onClick={this.onClickText}/>
										</a>
									);
								}.bind(this))
								: null
							}

							{!hasControl ? null :
								<a style={styles.modifyIcon} onClick={this.onClickButton.bind(null, 0)}><i className="grey link setting icon"></i></a>
							}

							{!hasControl && !this.props.onClickDelete ? null :
								<a style={styles.deleteIcon} onClick={this.onClickButton.bind(null, 1)}><i className="grey link remove icon"></i></a>
							}

							{!hasLoadingIndicator ? null :
								<a style={styles.loadingIcon}><i className="grey link notched circle loading icon"></i></a>
							}
						</div>
					</div>
				</div>

				{description.length === 0 ? null :
					<div className="ui hidden divider" style={styles.divider}></div>
				}

				{description.length === 0 ? null : <p/>}

				{description.length === 0 ? null : 
					description.map(function(descComp, index) {
						var comps = [<SearchCellText text={descComp}/>];
						if (index < description.length - 1) {
							comps.push(<br/>);
						}
						return comps;
					})
				}
			</div>
		);
	}
});

module.exports = SearchCell;

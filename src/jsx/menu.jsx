var React = require('react');

var Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var styles = {
	goBack: {
		paddingLeft: '6px'
	},
	request: {
		paddingLeft: '4px'
	},
	upload: {
		paddingLeft: '5px'
	}
};

var Menu = React.createClass({
	onClickGoBack: function() {
		location.href = '/';
	},

	onClickRequest: function() {
		location.href = '/request';
	},

	onClickInsert: function() {
		Actions.openInsertForm(true, { clean: true });

		process.nextTick(function() {
			Actions.dismissPopup();
		});
	},

	onClickUpload: function() {
		location.href = '/upload';
	},

	onClickMessage: function() {
		location.href = '/message-box';
	},

	onClickWhatsNew: function() {
		location.href = '/update-log';
	},

	render: function() {
		function getItem(iconName, title, style, onClick, hasDot) {
			return (
				<div className="item" onClick={onClick}>
					<i className={'large grey icon ' + iconName}></i>
					<div className="content" style={style}>
						<div className="header">{title}</div>
					</div>
					{hasDot === true ?
						<i className="small red icon circle"></i>
						: null
					}
				</div>
			);
		}

		var caller = this.props.caller;
		var items = [];

		if (caller !== Constants.CALLER.APP) {
			items.push( getItem('arrow left', 'Go back', styles.goBack, this.onClickGoBack) );
		}

		if (this.props.token === null) {
			if (caller !== Constants.CALLER.REQUEST) {
				items.push( getItem('send', 'Request', styles.request, this.onClickRequest) );
			}
		} else {
			if (caller === Constants.CALLER.APP) {
				items.push( getItem('plus', 'Insert', null, this.onClickInsert) );
			}

			if (caller !== Constants.CALLER.UPLOAD) {
				items.push( getItem('table', 'Upload', styles.upload, this.onClickUpload) );
			}

			if (caller !== Constants.CALLER.MESSAGE) {
				var hasDot = this.props.unreadMessageCount > 0;
				items.push( getItem('inbox', 'Message', null, this.onClickMessage, hasDot) );
			}
		}

		if (caller !== Constants.CALLER.UPDATE) {
			items.push( getItem('info', "What's new?", null, this.onClickWhatsNew) );
		}

		return (
			<div className="ui fluid popup top right transition hidden">
				<div className="ui middle aligned selection list">
					{items}
				</div>
			</div>
		);
	}
});

module.exports = Menu;

var React = require('react');

var Menu = require('./menu.jsx');

var Actions = require('../js/actions.js');

var styles = {
	header: {
		backgroundColor: 'rgba(255, 255, 255, 0.8)',
		height: '50px',
		borderBottom: '1px solid rgba(0, 0, 0, 0.2)'
	},
	logInIcon: {
		marginTop: '12px',
		marginLeft: '3px'
	},
	logOutIcon: {
		marginTop: '12px',
		marginLeft: '3px'
	},
	spinnerIcon: {
		marginTop: '15px',
		marginLeft: '3px'
	},
	homeIcon: {
		marginTop: '11px'
	},
	sidebarIcon: {
		marginTop: '11px',
		marginRight: '-1px'
	},
	sidebarIconWithDot: {
		marginTop: '11px',
		marginRight: '1px'
	}
};

var Header = React.createClass({
	onClickHome: function() {
		location.href = '/';
	},

	onClickLogInOut: function() {
		if (this.props.token !== null) {
			Actions.logOut({ redirect: this.props.shouldRedirect });
		} else {
			Actions.popUpLogInForm(true);
		}
	},

	getPopup: function() {
		return $('.column .icon.sidebar');
	},

	activatePopup: function() {
		this.getPopup().popup({
			hoverable: true,
			lastResort: true,
			popup: '.ui.popup',
			position: 'top right',
			setFluidWidth: false,
			maxSearchDepth: 30,
			distanceAway: 2,
			offset: 3,
			on: 'click'
		});
	},

	resizePopup: function() {
		$('.ui.popup').css('width', 164);
	},

	componentDidMount: function() {
		this.activatePopup();

		this.resizePopup();

		$(window).resize(function(event) {
			this.resizePopup();
		}.bind(this));

		$(window).scroll(function(event) {
			this.getPopup().popup('hide');
		}.bind(this));
	},

	componentWillReceiveProps: function(nextProps) {
		if (nextProps.shouldDismissPopup === true) {
			this.getPopup().popup('hide');
		}
	},

	componentWillUnmount: function() {
		this.getPopup().popup('destroy');
	},

	render: function() {
		var isFetchingToken = this.props.isFetchingToken;
		var token = this.props.token;
		var insertFormState = this.props.insertFormState;

		var logInClassName = 'big link grey icon toggle ';
		logInClassName += (this.props.token !== null ? 'on' : 'off');

		var unreadMessageCount = 0;
		if (this.props.messages !== undefined && this.props.messages !== null) {
			this.props.messages.forEach(function(message) {
				if (message.read === 0) {
					unreadMessageCount++;
				}
			});
		}

		return (
			<div className="ui fixed top sticky center aligned fluid container" style={styles.header}>
				<div className="ui container">
					<div className="ui center aligned equal width grid">
						<div className="left aligned column">
							{!isFetchingToken ? null :
								<i className="grey loading icon large notched circle" style={styles.spinnerIcon}></i>
							}

							{isFetchingToken || token === null ? null :
								<i className="big link grey icon toggle on" style={styles.logOutIcon} onClick={this.onClickLogInOut}></i>
							}

							{isFetchingToken || token !== null ? null :
								<i className="big link grey icon toggle off" style={styles.logInIcon} onClick={this.onClickLogInOut}></i>
							}
						</div>

						<div className="two wide center aligned column">
							<i className="big link grey icon home" style={styles.homeIcon} onClick={this.onClickHome}></i>
						</div>

						<div className="right aligned column">
							{unreadMessageCount > 0 ?
								<i className="big icons" style={styles.sidebarIconWithDot} onClick={this.onClickSidebar}>
									<i className="link grey icon sidebar"></i>
									<i className="link corner red icon circle"></i>
								</i>
								: 
								<i className="big link grey icon sidebar" style={styles.sidebarIcon} onClick={this.onClickSidebar}></i>
							}
						</div>
					</div>
				</div>

				<Menu token={token} caller={this.props.caller} unreadMessageCount={unreadMessageCount} />
			</div>
		);
	}
});

module.exports = Header;

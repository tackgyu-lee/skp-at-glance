var React = require('react');

var SearchCell = require('./search-cell.jsx');

var styles = {
	column: {
		marginBottom: '-28px'
	}
};

var SearchList = React.createClass({
	render: function() {
		var terms = this.props.terms;
		if (terms.length === 0) {
			return null;
		}

		var token = this.props.token;
		var insertFormState = this.props.insertFormState;

		var onClickDelete = this.props.onClickDelete;
		onClickDelete = onClickDelete === undefined ? null : onClickDelete;

		return (
			<div className="ui fluid grid">
				{terms.map(function(term, index) {
					if (term.deleted === true) {
						return null;
					}

					return (
						<div className="sixteen wide center aligned column" key={'cell-' + index} style={styles.column} >
							<SearchCell term={term} token={token} insertFormState={insertFormState} index={index} onClickDelete={onClickDelete}/>

							{index === terms.length - 1 ? null :
								<div className="ui divider"></div>
							}
						</div>
					);
				})}
			</div>
		);
	}
});

module.exports = SearchList;

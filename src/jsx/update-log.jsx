var React = require('react'),
	ReactDOM = require('react-dom'),
	moment = require('moment');

var Controller = require('../js/controller.js'),
	Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var Header = require('./header.jsx'),
	LogInForm = require('./login-form.jsx'),
	Footer = require('./footer.jsx');

var styles = {
	popUpGrid: {
		height: '100%'
	},
	popUpColumn: {
		maxWidth: '350px'
	},
	container: {
		marginTop: '58px',
		marginBottom: '35px'
	},
	createdAt: {
		color: '#767676',
		fontSize: '0.8em'
	},
	loadingSegment: {
		marginTop: '-30px',
		height: '120px'
	}
};

var UpdateLog = React.createClass({
	getInitialState: function() {
	    return Controller.getState();
	},

	onChange: function() {
		this.setState(Controller.getState());

		$(ReactDOM.findDOMNode(this.refs.logInForm))
			.dimmer(this.state.logInFormState.shouldPopUp === true ? 'show' : 'hide');
	},

	componentDidMount: function() {
		Controller.addChangeListener(this.onChange);

		Actions.getUpdateLogs();
		Actions.getVersion();
		Actions.getTokenFromSession();
	},

	componentWillUnmount: function() {
		Controller.removeChangeListener(this.onChange);
	},

	render: function() {
		var logs = this.state.updateLogs;

		function getDateString(dateString) {
			var m = moment(dateString);
			return m.format('YYYY-MM-DD HH:mm:ss');
		};

		return (
			<div ref="container">
				<div ref="logInForm" className="ui page dimmer">
					<div className="content">
						<div className="ui middle aligned center aligned grid" style={styles.popUpGrid}>
							<div className="column" style={styles.popUpColumn}>
								<LogInForm logInFormState={this.state.logInFormState}/>
							</div>
						</div>
					</div>
				</div>

				<Header isFetchingToken={this.state.isFetchingToken}
					token={this.state.token}
					messages={this.state.messageBoxState.messages}
					caller={Constants.CALLER.UPDATE} />

				<div className="ui container" style={styles.container}>
					<div className="ui fluid basic segment">
						{logs.map(function(log, index) {
							var comps = log.description.split('\n');
							
							var description = [];
							comps.forEach(function(comp, anIndex) {
								description.push(comp);
								if (anIndex < comps.length - 1) {
									description.push(<br/>);
								}
							});

							return (
								<div key={'update-log-' + index}>
									<h3>{log.version}</h3>
									<span style={styles.createdAt}>{getDateString(log.created_at)}</span><p/>
									{description}

									{index < logs.length - 1 ?
										<div className="ui divider"></div>
										:
										<div className="ui hidden divider"></div>
									}
								</div>
							);
						}.bind(this))}
					</div>

					{logs.length === 0 ?
						<div className="ui fluid basic loading segment" style={styles.loadingSegment}></div>
						: null
					}
				</div>

				<Footer version={this.state.version}/>
			</div>
		);
	}
});

module.exports = UpdateLog;




var React = require('react'),
	ReactDOM = require('react-dom');

var Controller = require('../js/controller.js'),
	Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var Header = require('./header.jsx'),
	Footer = require('./footer.jsx'),
	LogInForm = require('./login-form.jsx'),
	SearchList = require('./search-list.jsx');

var styles = {
	popUpGrid: {
		height: '100%'
	},
	popUpColumn: {
		maxWidth: '350px'
	},
	container: {
		marginTop: '38px',
		paddingBottom: '5px',
		marginLeft: '-28px',
		marginRight: '-28px'
	},
	uploader: {
		visibility: 'hidden',
		position: 'fixed',
	},
	form: {
		padding: '13px'
	},
	infoIdle: {
		color: '#abab8f'
	},
	infoFailed: {
		color: '#d0261f'
	},
	bottomGrid: {
		marginTop: '0px',
		marginBottom: '50px',
		marginLeft: '-28px',
		marginRight: '-28px'
	},
	bottomColumn: {
		marginTop: '15px',
		// marginLeft: '18px',
		// marginRight: '18px'
	},
	bottomDescColumn: {
		// marginLeft: '18px',
		// marginRight: '18px',
		marginBottom: '50px'
	},
	bottomButtonColumn: {
		paddingTop: '0px',
		marginLeft: '18px',
		marginRight: '18px'
	},
	bottomListColumn: {
		marginLeft: '18px',
		marginRight: '18px'
	},
	leftButton: {
		paddingLeft: '40px'
	},
	rightButton: {
		paddingRight: '40px'
	}
};

var Uploader = React.createClass({
	getInitialState: function() {
	    return Controller.getState();
	},

	onChange: function() {
		var nextState = Controller.getState();
		
		if (nextState.token === null) {
			location.href = '/';
		} else {
			this.setState(nextState);

			$(ReactDOM.findDOMNode(this.refs.logInForm))
				.dimmer(this.state.logInFormState.shouldPopUp === true ? 'show' : 'hide');

			process.nextTick(function() {
				Actions.getVersion();
			});
		}
	},

	onClickUpload: function() {
		var state = this.state.uploaderState.state;
		var terms = this.state.uploaderState.terms;

		if (state === 1) {
			return;
		} else if (state === 0 && terms.length > 0) {
			return;
		}

		$(this.refs.uploader).click();
	},

	onClickButton: function(index, event) {
		if (index === 0) {
			Actions.emptyTerms();
		} else if (index === 1) {
			Actions.updateTerms(this.state.uploaderState.terms);
		}
	},

	onClickDelete: function(termIndex) {
		Actions.deleteOnUploadableTerm(termIndex);
	},

	componentDidMount: function() {
		Actions.getTokenFromSession();
		Controller.addChangeListener(this.onChange);

		$(this.refs.uploader).on('change', this.validateFile);
	},

	validateFile: function(event) {
		var file = event.target.files[0];

		if (file.size > 10 * 1024 * 1024) {
			return;
		}

		var data = new FormData();
		data.append('uploads', file, file.name);

		process.nextTick(function() {
			Actions.uploadData(file.name, data);
		});
	},

	componentWillUnmount: function() {
		Controller.removeChangeListener(this.onChange);
	},

	render: function() {
		var filename = this.state.uploaderState.filename;
		var state = this.state.uploaderState.state;
		var terms = this.state.uploaderState.terms;

		var formClassName = 'ui form';
		if (state === 1) {
			formClassName += ' loading';
		}

		var fieldClassName = 'ui fluid big action input';
		if (state === 0 && terms.length > 0) {
			fieldClassName += ' disabled';
		}

		return (
			<div ref="container">
				<div ref="logInForm" className="ui page dimmer">
					<div className="content">
						<div className="ui middle aligned center aligned grid" style={styles.popUpGrid}>
							<div className="column" style={styles.popUpColumn}>
								<LogInForm logInFormState={this.state.logInFormState}/>
							</div>
						</div>
					</div>
				</div>

				<input ref="uploader" type="file" name="uploads" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style={styles.uploader}/>

				<Header isFetchingToken={this.state.isFetchingToken}
					token={this.state.token}
					shouldRedirect={true}
					messages={this.state.messageBoxState.messages}
					caller={Constants.CALLER.UPLOAD} />

				<div className="ui container">
					<div className="ui fluid grid" style={styles.container}>
						<div className="sixteen wide center aligned column">
							<div className={formClassName} style={styles.form}>
								<div className={fieldClassName}>
									<input ref="placeholder" type="text" value={filename} placeholder="upload .csv, .xls, .xlsx, ..."/>
									<button className="ui blue icon button" onClick={this.onClickUpload}>
										<i className="arrow up icon"></i>
									</button>
								</div>
							</div>
						</div>
					</div>

					{state === -1 ?
						<div className="sixteen wide column" style={styles.bottomColumn}>
							<span style={styles.infoFailed}>
								Failed to parse uploaded spreadsheet.<br/>
							</span>
						</div>
						: null
					}

					{state !== 0 || (state === 0 && terms.length === 0) ?
						<div className="sixteen wide column" style={styles.bottomDescColumn}>
							<span style={styles.infoIdle}>
								Format (from left to right):<br/>
								- type (number, optional, default = 0)<br/>
								- keyword (string, required)<br/>
								- title (string, required)<br/>
								- description (string, optional, default = null)<br/>
								- tags (string separated by pause mark (,), optional, default = empty array)<br/>
							</span>
						</div>
						: null
					}

					{state === 0 && terms.length > 0 ?
						<div className="ui fluid grid" style={styles.bottomGrid}>
							<div className="sixteen wide column" style={styles.bottomButtonColumn}>
								<span style={styles.infoIdle}>
									Information is fetched from spreadsheet you just have uploaded.<br/>
									Do you really want to upload the following list?<br/>
								</span>
							</div>

							<div className="eight wide center aligned column" style={styles.leftButton}>
								<button className="ui fluid button" onClick={this.onClickButton.bind(null, 0)}>cancel</button>
							</div>

							<div className="eight wide center aligned column" style={styles.rightButton}>
								<button className="ui primary fluid button" onClick={this.onClickButton.bind(null, 1)}>upload</button>
							</div>

							<div className="sixteen wide column" style={styles.bottomListColumn}>
								<SearchList terms={this.state.uploaderState.terms} onClickDelete={this.onClickDelete}/>
							</div>
						</div>
						: null
					}
				</div>

				<Footer version={this.state.version}/>
			</div>
		);
	}
});

module.exports = Uploader;

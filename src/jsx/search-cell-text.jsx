var React = require('react');

var Constants = require('../js/constants.js');

var PRE_TAG_HERE = 'PRE_TAG_HERE';

var SearchCellText = React.createClass({
	render: function() {
		var styles = {
			normal: {
				color: this.props.color
			},
			highlighted: {
				color: 'rgba(28, 182, 171, 1.0)'
			}
		};

		var onClick = (this.props.onClick === undefined || this.props.onClick === null) ? null : this.props.onClick;

		var text = this.props.text;

		var preTag = Constants.SEARCH.TAG.PRE;
		var postTag = Constants.SEARCH.TAG.POST;

		if (text.search(preTag) === -1) {
			return (
				<span style={styles.normal} onClick={onClick}>{text}</span>
			);
		}

		var array = [];

		text.split(preTag).forEach(function(component, index) {
			if (index === 0) {
				if (component.length === 0) {
					array.push(PRE_TAG_HERE);
				}
			} else {
				array.push(PRE_TAG_HERE);
			}

			component.split(postTag).forEach(function(aComponent) {
				if (aComponent.length === 0) {
					return;
				}
				array.push(aComponent);
			});
		});

		var indices = [];

		array.forEach(function(character, index) {
			if (character === PRE_TAG_HERE) {
				indices.push(index+1);
			}
		});

		return (
			<span onClick={onClick}>
				{array.map(function(character, index) {
					if (character === PRE_TAG_HERE) {
						return null;
					}

					var found = false;

					for (var i = 0; i < indices.length; i++) {
						if (indices[i] === index) {
							found = true;
							break;
						}
					}

					if (found) {
						return (
							<span key={'highlighted-' + index} style={styles.highlighted}>{character}</span>
						);
					} else {
						return (
							<span key={'highlighted-' + index} style={styles.normal}>{character}</span>
						);
					}
				})}
			</span>
		);
	}
});

module.exports = SearchCellText;
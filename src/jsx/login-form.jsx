var React = require('react'),
	ReactDOM = require('react-dom');

var Actions = require('../js/actions.js');

var styles = {
	field: {
		marginBottom: '15px'
	}
};

var LogInForm = React.createClass({
	getInitialState: function() {
		return this.emptyState();
	},

	emptyState: function() {
		return {
			username: '',
			password: '',
			isLoading: false
		};
	},

	onChange: function(index, event) {
		var text = event.target.value;

		if (index === 0) {
			this.setState({ username: text });
		} else if (index === 1) {
			this.setState({ password: text });
		}
	},

	onKeyPress: function(index, event) {
		if (event.key !== 'Enter') {
			return;
		}

		if (index === 0) {
			this.refs.password.focus();
		} else if (index === 1) {
			if (!this.state.isLoading) {
				this.beginLoggingIn();
			}
		}
	},

	onClick: function() {
		if (!this.state.isLoading) {
			this.beginLoggingIn();
		}
	},

	beginLoggingIn: function() {
		this.refs.username.blur();
		this.refs.password.blur();

		this.setState({ isLoading: true });

		Actions.logIn(this.state.username, this.state.password);
	},

	componentWillReceiveProps: function(nextProps) {
		var nextState = this.state;

		var shouldSetState = false;
		var shouldShake = false;
		var shouldFocusInput = false;

		if (nextProps.logInFormState.shouldEmpty) {
			nextState = this.emptyState();
			shouldSetState = true;
		}

		if (nextProps.logInFormState.shouldInformFailure) {
			nextState.isLoading = false;
			shouldSetState = true;
			shouldShake = true;
		}

		if (nextProps.logInFormState.shouldPopUp) {
			shouldFocusInput = true;
		}

		if (shouldSetState) {
			this.setState(nextState);
		}

		process.nextTick(function() {
			if (shouldShake) {
				$(ReactDOM.findDOMNode(this.refs.form)).transition('shake');
			}

			if (shouldFocusInput) {
				this.refs.username.focus();
			}
		}.bind(this));
	},

	// componentDidUpdate: function() {
	// 	if (this.props.logInFormState.shouldPopUp) {
	// 		this.refs.username.focus();
	// 	}
	// },

	render: function() {
		var inputClassName = 'ui fluid input';
		var buttonClassName = 'ui button';

		if (this.state.isLoading) {
			inputClassName += ' disabled';
			buttonClassName += ' loading';
		}

		return (
			<div ref="form" className="ui center aligned segment">
				<h3 className="ui header">Log In</h3>

				<div className={inputClassName} style={styles.field}>
					<input type="text" name="first-name" ref="username" placeholder="username" value={this.state.username} onChange={this.onChange.bind(null, 0)} onKeyPress={this.onKeyPress.bind(null, 0)}/>
				</div>

				<div className={inputClassName} style={styles.field}>
					<input type="password" name="last-name" ref="password" placeholder="password" value={this.state.password} onChange={this.onChange.bind(null, 1)} onKeyPress={this.onKeyPress.bind(null, 1)}/>
				</div>

				<div className="ui grid">
					<div className="sixteen wide column">
						<button className={buttonClassName} onClick={this.onClick}>Submit</button>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = LogInForm;


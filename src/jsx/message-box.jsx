var React = require('react'),
	ReactDOM = require('react-dom');

var Controller = require('../js/controller.js'),
	Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var Header = require('./header.jsx'),
	Footer = require('./footer.jsx'),
	LogInForm = require('./login-form.jsx'),
	MessageForm = require('./message-form.jsx');

var styles = {
	popUpGrid: {
		height: '100%'
	},
	popUpColumn: {
		maxWidth: '350px'
	},
	container: {
		marginTop: '65px',
		marginBottom: '32px',
	},
	segment: {
		padding: '0px'
	},
	header: {
		marginTop: '5px'
	}
};

var MessageBox = React.createClass({
	getInitialState: function() {
	    return Controller.getState();
	},

	onChange: function() {
		this.setState(Controller.getState());

		$(ReactDOM.findDOMNode(this.refs.logInForm))
			.dimmer(this.state.logInFormState.shouldPopUp === true ? 'show' : 'hide');
	},

	onClickMessage: function(index, event) {
		Actions.showMessage(index);
	},

	componentDidMount: function() {
		Controller.addChangeListener(this.onChange);

		Actions.getVersion();
		Actions.getTokenFromSession();
	},

	componentWillUnmount: function() {
		Controller.removeChangeListener(this.onChange);
	},

	render: function() {
		var messageBoxState = this.state.messageBoxState;

		var isLoading = messageBoxState.isLoading;
		var messages = messageBoxState.messages;
		var logMessage = messageBoxState.logMessage;
		var index = messageBoxState.index;

		var isEmpty = messages.length === 0;

		return (
			<div ref="container">
				<div ref="logInForm" className="ui page dimmer">
					<div className="content">
						<div className="ui middle aligned center aligned grid" style={styles.popUpGrid}>
							<div className="column" style={styles.popUpColumn}>
								<LogInForm logInFormState={this.state.logInFormState}/>
							</div>
						</div>
					</div>
				</div>

				<Header isFetchingToken={this.state.isFetchingToken}
					token={this.state.token}
					shouldRedirect={true}
					messages={this.state.messageBoxState.messages}
					caller={Constants.CALLER.MESSAGE} />

				<div className="ui container">
					<div className={'ui basic segment' + (isLoading ? ' loading' : '')} style={styles.segment}>

						<div className="ui fluid grid" style={styles.container}>
							<div className={'sixteen wide column' + (isEmpty ? ' center aligned' : '')}>
								{isEmpty ?
									<i className="massive icon grey inbox"></i>
									: null
								}

								{isEmpty ?
									<h3 className="ui header grey" style={styles.header}>{logMessage}</h3>
									: null
								}

								{messages.map(function(message, _index) {
									var components = [
										<MessageForm isActive={_index === index} message={message} onClick={this.onClickMessage.bind(null, _index)} />
									];

									if (_index < messages.length - 1) {
										components.push(<div className="ui divider"></div>);
									}

									return components;
								}.bind(this))}
							</div>
						</div>

					</div>
				</div>

				<Footer version={this.state.version}/>
			</div>
		);
	}
});

module.exports = MessageBox;

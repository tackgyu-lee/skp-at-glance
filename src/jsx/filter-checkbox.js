var React = require('react');

var Constants = require('../js/constants.js');

var styles = {
	label: {
		paddingLeft: '20px'
	}
};

var FilterCheckbox = React.createClass({
	onClick: function(index, event) {
		this.props.onChange(index);
	},

	onChange: function() {
		// dummy onChange
	},

	render: function() {
		if (this.props.filterState.state !== Constants.FILTER.LOADED) {
			return null;
		}

		return (
			<div className="ui form">
				<div className="inline fields">
					<label for="fruit">group</label>

					<div className="field">
						<div className="ui radio checkbox" onClick={this.onClick.bind(null, 0)}>
							<input type="radio" name="fruit" checked={this.props.index === 0 ? 'checked' : null} tabindex="0" className="hidden" onChange={this.onChange}/>
							<label style={styles.label}>none</label>
						</div>
					</div>

					{this.props.filterState.specs.map(function(spec, index) {
						return (
							<div className="field" key={'checkbox-' + index}>
								<div className="ui radio checkbox" onClick={this.onClick.bind(null, index+1)}>
									<input type="radio" name="fruit" checked={this.props.index === (index+1) ? 'checked' : null} tabindex="0" className="hidden" onChange={this.onChange}/>
									<label style={styles.label}>{spec.title}</label>
								</div>
							</div>
						);
					}.bind(this))}
				</div>
			</div>
		);
	}
});

module.exports = FilterCheckbox;
var React = require('react');

var styles = {
	grid: {
		paddingTop: '20px',
		paddingBottom: '50px',
		borderTop: '1px solid rgba(0, 0, 0, 0.2)'
	},
	text: {
		color: '#bbbbbb'
	}
}

var Footer = React.createClass({
	render: function() {
		var version = this.props.version;

		return (
			<div id="footer" className="ui fluid container" style={styles.grid}>
				<div className="ui center aligned container">
					<div className="ui tiny image">
						<img src="../../skp_logo.png"/>
					</div>

					<p/>
					{version === undefined || version === null || typeof version !== 'string' || version.length === 0 ?
						null
						:
						<span style={styles.text}>{'v.' + version}</span>
					}

					<br/>
					<span style={styles.text}>made by @tackgyu.lee</span>
				</div>
			</div>
		);
	}
});

module.exports = Footer;



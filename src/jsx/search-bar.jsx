var React = require('react');

var Actions = require('../js/actions.js');

var styles = {
	cross: {
		marginRight: '30px'
	}
};

var SearchBar = React.createClass({
	cached: '',

    onChange: function(event) {
    	var text = event.target.value;
    	var pending = false;

    	if (text.substr(text.length - 1, 1) === ' ') {
    		pending = true;
    	} else if (text.length > 0 && this.cached.substr(0, this.cached.length - 1) === text) {
    		var last = this.cached.substr(this.cached.length - 1, 1);
    		pending = last === ' ' || last === '@' || last === '#';
    	} else {
	    	var split = text.trim().split(' ');
	    	for (var i = 0; i < split.length; i++) {
	    		var trimmed = split[i].trim();
	    		if (trimmed === '@' || trimmed === '#') {
	    			pending = true;
	    			break;
	    		}
	    	}
    	}

    	this.cached = text;
    	Actions.search(text, { pending: pending, typing: true });
    },

    onDelete: function() {
    	Actions.search('');
    },

    componentDidMount: function() {
    	this.refs.input.focus();
    },

	render: function() {
		var inputClassName = 'ui big icon fluid input';
		if (this.props.isSearching) {
			inputClassName += ' loading';
		}

		var placeholder = 'keyword, @group, #tag, ...';

		var searchText = this.props.searchText;
		var noText = searchText === undefined || searchText === null || searchText.length === 0;

		return (
			<div className={inputClassName}>
				<input ref="input" type="text" placeholder={placeholder} value={this.props.searchText} onChange={this.onChange}/>
				{(this.props.isSearching || noText) ? null :
					<i className="remove circle grey link icon" style={styles.cross} onClick={this.onDelete}></i>
				}
				<i className="search grey link icon" onClick={this.onChange}></i>
			</div>
		);
	}
});

module.exports = SearchBar;
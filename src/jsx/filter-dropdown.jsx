var React = require('react');

var Actions = require('../js/actions.js'),
	Constants = require('../js/constants.js');

var styles = {
	segment: {
		marginLeft: '-14px',
		width: '100%'
	}
};

var FilterDropdown = React.createClass({
	onClick: function() {
		if (this.props.filterState.state === Constants.FILTER.FAILED) {
			Actions.getFilters();
		}
	},

	componentDidMount: function() {
		var menu = $('.ui.dropdown');

		menu.dropdown('setting', 'onChange', function() {
			var value = menu.dropdown('get value');
			var filterIndex = parseInt(value);

			if (isNaN(filterIndex)) {
				Actions.setFilterIndex(-1);
			} else {
				Actions.setFilterIndex(filterIndex);
			}
		}).dropdown('restore defaults');
	},
	
	render: function() {
		var filterState = this.props.filterState;

		var segmentClassName = 'ui basic compact segment';
		if (filterState.state === Constants.FILTER.INIT) {
			segmentClassName += ' loading';
		}

		var dropdownClassName = 'ui selection dropdown';
		if (filterState.state === Constants.FILTER.FAILED) {
			dropdownClassName += ' error';
		}

		return (
			<div className={segmentClassName} style={styles.segment}>
				<div className={dropdownClassName} onClick={this.onClick}>
					<input type="hidden" name="filters"/>
					<i className="dropdown icon"></i>
					<div className="default text">Filter</div>
					<div className="menu">
						{filterState.state !== Constants.FILTER.LOADED ? null :
							<div className="item" data-value="0">all</div>
						}
						{filterState.specs.map(function(spec, index) {
							if (filterState.state !== Constants.FILTER.LOADED) {
								return null;
							}

							return (
								<div className="item" data-value={'' + (index + 1)} key={'filter-' + index}>{spec.title}</div>
							);
						}.bind(this))}
					</div>
				</div>
			</div>
		);
	}
});

module.exports = FilterDropdown;

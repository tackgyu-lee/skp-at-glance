var React = require('react'),
	ReactDOM = require('react-dom');

var UpdateLog = require('./jsx/update-log.jsx');

ReactDOM.render(
	<UpdateLog />, document.getElementById('log')
);


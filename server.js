var express = require('express'),
	cookieParser = require('cookie-parser'),
	session = require('express-session'),
	bodyParser = require('body-parser'),
	formidable = require('formidable'),
	request = require('request');

var db = require('./src/js/database.js'),
	sessionKit = require('./src/js/session-kit.js'),
	searchKit = require('./src/js/search-kit.js'),
	excelKit = require('./src/js/excel-kit.js');

var Constants = require('./src/js/constants.js');

var server = {
	secret: null,

	version: function(req, res) {
		res.status(200).json({ version: process.env.APP_VERSION });
	},

	checkSession: function(req, res) {
		console.log('[Server] request: session check');

		sessionKit.verify(server.secret, req.session.token)
			.then(function(decoded) {
				var user = decoded.target;
				console.log('[Server] session check: verified, user = ' + user);

	 			db.setUserLogInDate(user);

	 			var token = sessionKit.generate(server.secret, user);
	 			req.session.token = token;

	 			res.status(200).json({ token: token });
			}, function(err) {
				console.log('[Server] error while verifying token');
				req.session.token = null;

	 			res.status(400).json({ error: err });
			});
	},

	logOut: function(req, res) {
		console.log('[Server] request: session logOut');
		req.session.token = null;
		res.status(200).end();
	},

	logIn: function(req, res) {
		var username = req.body.username;
 		var password = req.body.password;

		console.log('[Server] request: logIn: username = ' + username + ', password = ' + password);

 		if (username === undefined) {
			console.log('[Server] invalid username');
 			res.status(400).json({ error: 'Invalid username' });
 		} else if (password === undefined) {
			console.log('[Server] invalid password');
 			res.status(400).json({ error: 'Invalid password' });
 		} else {
	 		var user = {
	 			username: username,
	 			password: password
	 		};

			console.log('[Server] search for user = ' + user);

	 		db.getUser(user).then(function(user) {
				console.log('[Server] user found, update log-in date');
				
	 			db.setUserLogInDate(user);

	 			var token = sessionKit.generate(server.secret, user);
				req.session.token = token;

	 			res.status(200).json({ token: token });
	 		}, function(err) {
				console.log('[Server] user not found');
	 			res.status(400).json({ error: err });
	 		});
 		}
	},

	filter: function(req, res) {
		console.log('[Server] request: filter');

		db.getFilters().then(function(filters) {
			res.status(200).json({ filters: filters });
		}, function(err) {
			res.status(400).json({ error: err});
		});
	},

	search: function(req, res) {
		console.log('[Server] request: search with ' + req.body);

		var page = req.body.page;
		var matches = req.body.matches;

		searchKit.run(page, matches).then(function(results) {
			res.status(200).json({ results: results });
		}, function(err) {
			res.status(400).json({ error: err});
		});
	},

	update: function(req, res) {
		console.log('[Server] request: update');

		sessionKit.verify(server.secret, req.session.token)
			.then(function(decoded) {
				return db.updateTerms(req.body);
			}, function(err) {
				console.log('[Server] invalid user request');
				res.status(400).json({ error: 'Invalid user request' });
				return Promise.reject();
			}).then(function(results) {
				console.log(results);
				var successCount = 0;
				var totalCount = 0;

				results.forEach(function(result) {
					if (result.code === 0) {
						successCount++;
					}
					totalCount++;
				});

				var code = 200;

				if (successCount === 0) {
					console.log('[Server] update finished with error');
					code = 400;
				} else if (successCount < totalCount) {
					console.log('[Server] update finished partially');
					code = 206;
				} else {
					console.log('[Server] update succeeded');
				}

				console.log(code, results);

				res.status(code).json({ successCount: successCount, results: results });
			});
	},

	upload: function(req, res) {
		console.log('[Server] request: upload');

		sessionKit.verify(server.secret, req.session.token)
			.then(function(decoded) {
				return excelKit.root(__dirname).clean()
			}, function(err) {
				console.log('[Server] invalid user request');
				res.status(400).json({ error: 'Invalid user request' });
				return Promise.reject();
			}).then(function(dir) {
				return new Promise(function(resolve, reject) {
					var form = new formidable.IncomingForm();

					form.multiples = false;
					form.uploadDir = dir;

					form.on('file', function(field, file) {
						console.log('[Server] file fetched: "' + file.name + '"');
						resolve(file);
					});

					form.on('error', function(err) {
						console.log('[Server] error occured: ' + err);
					});

					form.on('end', function() {
						console.log('[Server] finished');
					});

					console.log('[Server] start parsing incoming form data');
					form.parse(req);
				});
			}).then(function(file) {
				return excelKit.parse(file);
			}).then(function(results) {
				db.getFilters().then(function(filters) {
					results.forEach(function(result) {
						result.typeName = result.type === 0 ? '' : filters[result.type-1].title;
					});
					res.status(200).json({ results: results });
				}, function(err) {
					res.status(400).json({ error: err});
				});
			}, function(err) {
				res.status(400).json({ error: err });
			});
	},

	getLogs: function(req, res) {
		db.getLogs().then(function(logs) {
			res.status(200).json(logs);
		});
	},

	postLogs: function(req, res) {
		sessionKit.verify(server.secret, req.session.token)
			.then(function(decoded) {
				return db.postLogs(req.body);
			}, function(err) {
				res.status(400).json({ error: err });
			}).then(function(result) {
				res.status(200).json({ result: result });
			}, function(err) {
				res.status(400).json({ error: err });
			});
	},

	getMessages: function(req, res) {
		sessionKit.verify(server.secret, req.session.token)
			.then(function(decoded) {
				return db.getMessages();
			}, function(err) {
				res.status(400).json({ error: 'Invalid user request' });
				return Promise.reject();
			}).then(function(logs) {
				res.status(200).json(logs);
			});
	},

	postMessages: function(req, res) {
		db.postMessages(req.body).then(function(result) {
			if (req.body.notify === true) {
				var color = '#36a64f';
				var title = 'General Purpose';

				if (req.body.kind == 1) {
					color = '#66b2ff';
					title = 'New Keyword Suggestion';
				}

				var payload = {
					attachments: [
						{
							fallback: 'Incoming request from <https://skp-at-glance.herokuapp.com>',
							pretext: 'Incoming request from <https://skp-at-glance.herokuapp.com>',
							color: color,
							fields: [
								{
									title: title,
									value: req.body.content
								}
							]
						}
					]
				};

				var options = {
					uri: Constants.SLACK_HOOK_URL,
					form: JSON.stringify(payload)
				};

				request.post(options, function(error, response, body) {
					res.status(200).json({ result: result });
				});
			} else {
				res.status(200).json({ result: result });
			}
		}, function(err) {
			res.status(400).json({ error: err });
		});
	},

	start: function(secret) {
		var portNumber = Constants.SERVER.PORT_NUM;
		server.secret = secret;

		express()
			.use(bodyParser.json())
			.use(bodyParser.urlencoded({
				extended: false,
			}))
			.use(cookieParser())
			.use(session({
				secret: secret,
				resave: false,
				saveUninitialized: true
			}))
			.use(express.static(__dirname + '/'))

			.get('/version', this.version)
			.get('/session', this.checkSession)
			.delete('/session', this.logOut)
			.put('/user', this.logIn)
			.get('/filter', this.filter)
			.put('/search', this.search)
			.post('/term', this.update)
			.post('/upload', this.upload)
			.get('/log', this.getLogs)
			.post('/log', this.postLogs)
			.get('/message', this.getMessages)
			.post('/message', this.postMessages)

			.get('/', function(req, res) {
				res.sendFile(__dirname + '/index.html');
			})

			.get('/update-log', function(req, res) {
				res.sendFile(__dirname + '/log.html');
			})

			.get('/message-box', function(req, res) {
				sessionKit.verify(server.secret, req.session.token).then(function(decoded) {
					res.sendFile(__dirname + '/message.html');
				}, function(err) {
					res.sendFile(__dirname + '/index.html');
				});
			})

			.get('/upload', function(req, res) {
				sessionKit.verify(server.secret, req.session.token).then(function(decoded) {
					res.sendFile(__dirname + '/upload.html');
				}, function(err) {
					res.sendFile(__dirname + '/index.html');
				});
			})

			.get('/request', function(req, res) {
				sessionKit.verify(server.secret, req.session.token).then(function(decoded) {
					res.sendFile(__dirname + '/index.html');
				}, function(err) {
					res.sendFile(__dirname + '/request.html');
				});
			})

			.listen(portNumber, function() {
				console.log('[Server] server is listening on port #' + portNumber);
			});
	}
};

var retryCount = 0;

var initServer = function() {
	console.log('[Server] fetching secret from database');

	db.getSecret().then(function(secret) {
		console.log('[Server] successfully fetched secret from database');
		console.log('[Server] initializing server');
		server.start(secret.string);
	}, function(err) {
		retryCount++;
		console.log('[Server] fetching secret failed (Retry count = ' + retryCount + ')');

		if (Constants.SERVER.MAX_RETRY < 3) {
			console.log('[Server] retry in ' + Constants.SERVER.RETRY_INTERVAL + ' seconds');

			setTimeout(function() {
				process.nextTick(function() {
					initServer();
				});
			}, Constants.SERVER.RETRY_INTERVAL * 1000);
		} else {
			console.log('[Server] failed to initialize server');
		}
	})
};

initServer();
